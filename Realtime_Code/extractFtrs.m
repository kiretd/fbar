function features = extractFtrs(data,Fs,type,filters)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Returns a vector of features which are used for either EOG or EMG
% artifact detection.
%
% USAGE:
%   features = artifactFeatureExtractor(data,Fs,type,filters)
%   
%   Output:
%       features (numerical): vector of features
%   Input:
%       data (numerical): vector of eeg data from one channel
%       Fs (numerical): scaler value denoting sampling frequency
%       type (string): 'EOG' or 'EMG'
%       filters (struct): the filterbank coefficients given by
%       initArtifactFilters
%
% Written by Kiret Dhindsa
% Last update: Nov 24, 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


switch type
    case 'EOG'
        % Separate eeg data into each component
        sig1 = filter(filters.filt1.b,filters.filt1.a,data);
        sig2 = filter(filters.filt2.b,filters.filt2.a,data);
        sig3 = filter(filters.filt3.b,filters.filt3.a,data);
        sig4 = filter(filters.filt4.b,filters.filt4.a,data);
        sig5 = filter(filters.filt5.b,filters.filt5.a,data);
        
%         % features for base signal (1-48 Hz)
%         max_amp = max(abs(sig1),[],1);
%         std_amp = std(sig1,[],1);
%         kurt = kurtosis(sig1,[],1);
%         skew = skewness(sig1,[],1);
%         ftrs1 = cat(1,max_amp,std_amp,kurt,skew);
        
        % features from low frequencies (0.5-8 Hz)
        pow = calcFFT(data,Fs,0.5,8);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig2),[],1);
        std_amp = std(sig2,[],1);
        kurt = kurtosis(sig2,[],1);
        skew = skewness(sig2,[],1);
        ftrs2 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from alpha frequencies (8-13 Hz)
        pow = calcFFT(data,Fs,8,13);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig3),[],1);
        std_amp = std(sig3,[],1);
        kurt = kurtosis(sig3,[],1);
        skew = skewness(sig3,[],1);
        ftrs3 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from beta1 frequencies (13-18 Hz)
        pow = calcFFT(data,Fs,13,18);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig4),[],1);
        std_amp = std(sig4,[],1);
        kurt = kurtosis(sig4,[],1);
        skew = skewness(sig4,[],1);
        ftrs4 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from beta2 frequencies (18-25 Hz)
        pow = calcFFT(data,Fs,18,25);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig5),[],1);
        std_amp = std(sig5,[],1);
        kurt = kurtosis(sig5,[],1);
        skew = skewness(sig5,[],1);
        ftrs5 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % Output final features
%         features = cat(1,ftrs1,ftrs2,ftrs3,ftrs4,ftrs5);
        features = cat(1,ftrs2,ftrs3,ftrs4,ftrs5);
        
    case 'EMG'
        % Separate eeg data into each component
        sig1 = data;
        sig2 = filter(filters.filt1.b,filters.filt1.a,data);
        sig3 = filter(filters.filt2.b,filters.filt2.a,data);
        sig4 = filter(filters.filt3.b,filters.filt3.a,data);
        
        % features for base signal
        max_amp = max(abs(sig1),[],1);
        std_amp = std(sig1,[],1);
        kurt = kurtosis(sig1,[],1);
        skew = skewness(sig1,[],1);
        ftrs1 = cat(1,max_amp,std_amp,kurt,skew);
        
        % features from low gamma (30-45 Hz)
        pow = calcFFT(data,Fs,30,45);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig2),[],1);
        std_amp = std(sig2,[],1);
        kurt = kurtosis(sig2,[],1);
        skew = skewness(sig2,[],1);
        ftrs2 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from mid gamma (65-80 Hz)
        pow = calcFFT(data,Fs,65,80);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig3),[],1);
        std_amp = std(sig3,[],1);
        kurt = kurtosis(sig3,[],1);
        skew = skewness(sig3,[],1);
        ftrs3 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from high gamma (80-100 Hz)
        pow = calcFFT(data,Fs,80,100);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig4),[],1);
        std_amp = std(sig4,[],1);
        kurt = kurtosis(sig4,[],1);
        skew = skewness(sig4,[],1);
        ftrs4 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % Output final features
        features = cat(1,ftrs1,ftrs2,ftrs3,ftrs4);
        
    case 'All'
        % Separate eeg data into each component
        sig1 = filter(filters.filt1.b,filters.filt1.a,data);
        sig2 = filter(filters.filt2.b,filters.filt2.a,data);
        sig3 = filter(filters.filt3.b,filters.filt3.a,data);
        sig4 = filter(filters.filt4.b,filters.filt4.a,data);
        sig5 = filter(filters.filt5.b,filters.filt5.a,data);
        sig6 = filter(filters.filt6.b,filters.filt6.a,data);
        sig7 = filter(filters.filt7.b,filters.filt7.a,data);
        sig8 = filter(filters.filt8.b,filters.filt8.a,data);
        sig9 = filter(filters.filt9.b,filters.filt9.a,data);
        
        % features from low frequencies (0.5-4 Hz)
        pow = calcFFT(data,Fs,0.5,4);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig1),[],1);
        std_amp = std(sig1,[],1);
        kurt = kurtosis(sig1,[],1);
        skew = skewness(sig1,[],1);
        ftrs1 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from alpha frequencies (4-8 Hz)
        pow = calcFFT(data,Fs,4,8);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig2),[],1);
        std_amp = std(sig2,[],1);
        kurt = kurtosis(sig2,[],1);
        skew = skewness(sig2,[],1);
        ftrs2 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from beta1 frequencies (8-13 Hz)
        pow = calcFFT(data,Fs,8,13);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig3),[],1);
        std_amp = std(sig3,[],1);
        kurt = kurtosis(sig3,[],1);
        skew = skewness(sig3,[],1);
        ftrs3 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from beta2 frequencies (13-18 Hz)
        pow = calcFFT(data,Fs,13,18);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig4),[],1);
        std_amp = std(sig4,[],1);
        kurt = kurtosis(sig4,[],1);
        skew = skewness(sig4,[],1);
        ftrs4 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from beta2 frequencies (18-25 Hz)
        pow = calcFFT(data,Fs,18,25);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig5),[],1);
        std_amp = std(sig5,[],1);
        kurt = kurtosis(sig5,[],1);
        skew = skewness(sig5,[],1);
        ftrs5 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from beta2 frequencies (25-30 Hz)
        pow = calcFFT(data,Fs,25,30);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig6),[],1);
        std_amp = std(sig6,[],1);
        kurt = kurtosis(sig6,[],1);
        skew = skewness(sig6,[],1);
        ftrs6 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from low gamma (30-45 Hz)
        pow = calcFFT(data,Fs,30,45);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig7),[],1);
        std_amp = std(sig7,[],1);
        kurt = kurtosis(sig7,[],1);
        skew = skewness(sig7,[],1);
        ftrs7 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from mid gamma (65-80 Hz)
        pow = calcFFT(data,Fs,65,80);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig8),[],1);
        std_amp = std(sig8,[],1);
        kurt = kurtosis(sig8,[],1);
        skew = skewness(sig8,[],1);
        ftrs8 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        % features from high gamma (80-100 Hz)
        pow = calcFFT(data,Fs,80,100);
        mean_pow = mean(pow,1);
        std_pow = std(pow,[],1);
        max_amp = max(abs(sig9),[],1);
        std_amp = std(sig9,[],1);
        kurt = kurtosis(sig9,[],1);
        skew = skewness(sig9,[],1);
        ftrs9 = cat(1,mean_pow,std_pow,max_amp,std_amp,kurt,skew);
        
        features = cat(1,ftrs1,ftrs2,ftrs3,ftrs4,ftrs5,ftrs6,ftrs7,ftrs8,ftrs9);
end