filters = initFilters('All',220);

t = zeros(1000,1);
t2 = zeros(1000,1);
for i = 1:1000
    data = rand(220,1);
    tic;
    output = detectArtifact(data,220,filters,Model);
    t(i) = toc;
end


for i = 1:1000
    data = rand(220,1);
    tic;
    output = extractFtrs(data,220,'All',filters);
    t2(i) = toc;
end