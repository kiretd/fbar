function artifact = detectArtifact(data,Fs,filters,model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Returns a 1 if if the data contains an artifact and 0 otherwise.
%
% USAGE:
%   artifact = detectArtifacts(data,Fs,EOGfilters,EMGfilters,EOGmodel,EMGmodel)
%   
%   Output:
%       artifact (numerical): 1 or 0 indicating presence of artifact
%   Input:
%    - data (numerical): vector of eeg data from one channel
%    - Fs (numerical): scaler value denoting sampling frequency
%    - filters (struct): the filterbank coefficients given by initFilters
%    - model (struct): contains all necessary model information for
%       classification

% Written by Kiret Dhindsa
% Last update: August 18, 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

features = extractFtrs(data,Fs,'All',filters);
features(isnan(features)) = 100;

% Normalize features according to model statistics
features = features - model.ftrStats.mean;
features = features ./model.ftrStats.std;

% Extract selected features
if isvector(model.ftrStats.selected)
    features = features(model.ftrStats.selected);
end

% Classify
artifact = svmclassify(model,features');
% artifact = -(artifact-1);

% return if EOG artifact detected (no need to check EMG)
if artifact == 1
    return;
end


