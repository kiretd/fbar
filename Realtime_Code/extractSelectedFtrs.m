function ftrs = extractSelectedFtrs(data,Fs,filters,selected,ftrmean,ftrstd)
% Computes only the features needed to run the Artifact Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = length(selected);
ftrs = zeros(N,1);

% Separate signal using filter-bank
sig1 = filter(filters.filt1.b,filters.filt1.a,data);
sig2 = filter(filters.filt2.b,filters.filt2.a,data);
sig3 = filter(filters.filt3.b,filters.filt3.a,data);
sig4 = filter(filters.filt4.b,filters.filt4.a,data);
sig5 = filter(filters.filt5.b,filters.filt5.a,data);
sig6 = filter(filters.filt6.b,filters.filt6.a,data);
sig7 = filter(filters.filt7.b,filters.filt7.a,data);
sig8 = filter(filters.filt8.b,filters.filt8.a,data);
sig9 = filter(filters.filt9.b,filters.filt9.a,data);

% Compute selected features in the order they were selected
for i = 1:N
    switch selected(i)
        case 1
            ftrs(i) = mean(calcFFT(sig1,Fs,0.5,4));
            ftrs(i) = (ftrs(i)-ftrmean(1))/ftrstd(1);
        case 2
            ftrs(i) = std(calcFFT(sig1,Fs,0.5,4),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(2))/ftrstd(2);
        case 3
            ftrs(i) = max(abs(sig1));
            ftrs(i) = (ftrs(i)-ftrmean(3))/ftrstd(3);
        case 4
            ftrs(i) = std(sig1);
            ftrs(i) = (ftrs(i)-ftrmean(4))/ftrstd(4);
        case 5
            ftrs(i) = kurtosis(sig1);
            ftrs(i) = (ftrs(i)-ftrmean(5))/ftrstd(5);
        case 6
            ftrs(i) = skewness(sig1);
            ftrs(i) = (ftrs(i)-ftrmean(6))/ftrstd(6);
        case 7
            ftrs(i) = mean(calcFFT(sig2,Fs,4,8));
            ftrs(i) = (ftrs(i)-ftrmean(7))/ftrstd(7);
        case 8
            ftrs(i) = std(calcFFT(sig2,Fs,4,8),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(8))/ftrstd(8);
        case 9
            ftrs(i) = max(abs(sig2));
            ftrs(i) = (ftrs(i)-ftrmean(9))/ftrstd(9);
        case 10
            ftrs(i) = std(sig2);
            ftrs(i) = (ftrs(i)-ftrmean(10))/ftrstd(10);
        case 11
            ftrs(i) = kurtosis(sig2);
            ftrs(i) = (ftrs(i)-ftrmean(11))/ftrstd(11);
        case 12
            ftrs(i) = skewness(sig2);
            ftrs(i) = (ftrs(i)-ftrmean(12))/ftrstd(12);
        case 13
            ftrs(i) = mean(calcFFT(sig3,Fs,8,13));
            ftrs(i) = (ftrs(i)-ftrmean(13))/ftrstd(13);
        case 14
            ftrs(i) = std(calcFFT(sig3,Fs,8,13),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(14))/ftrstd(14);
        case 15
            ftrs(i) = max(abs(sig3));
            ftrs(i) = (ftrs(i)-ftrmean(15))/ftrstd(15);
        case 16
            ftrs(i) = std(sig3);
            ftrs(i) = (ftrs(i)-ftrmean(16))/ftrstd(16);
        case 17
            ftrs(i) = kurtosis(sig3);
            ftrs(i) = (ftrs(i)-ftrmean(17))/ftrstd(17);
        case 18
            ftrs(i) = skewness(sig3);
            ftrs(i) = (ftrs(i)-ftrmean(18))/ftrstd(18);
        case 19
            ftrs(i) = mean(calcFFT(sig4,Fs,13,18));
            ftrs(i) = (ftrs(i)-ftrmean(19))/ftrstd(19);
        case 20
            ftrs(i) = std(calcFFT(sig4,Fs,13,18),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(20))/ftrstd(20);
        case 21
            ftrs(i) = max(abs(sig4));
            ftrs(i) = (ftrs(i)-ftrmean(21))/ftrstd(21);
        case 22
            ftrs(i) = std(sig4);
            ftrs(i) = (ftrs(i)-ftrmean(22))/ftrstd(22);
        case 23
            ftrs(i) = kurtosis(sig4);
            ftrs(i) = (ftrs(i)-ftrmean(23))/ftrstd(23);
        case 24
            ftrs(i) = skewness(sig4);
            ftrs(i) = (ftrs(i)-ftrmean(24))/ftrstd(24);
        case 25
            ftrs(i) = mean(calcFFT(sig5,Fs,18,25));
            ftrs(i) = (ftrs(i)-ftrmean(25))/ftrstd(25);
        case 26
            ftrs(i) = std(calcFFT(sig5,Fs,18,25),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(26))/ftrstd(26);
        case 27
            ftrs(i) = max(abs(sig5));
            ftrs(i) = (ftrs(i)-ftrmean(27))/ftrstd(27);
        case 28
            ftrs(i) = std(sig5);
            ftrs(i) = (ftrs(i)-ftrmean(28))/ftrstd(28);
        case 29
            ftrs(i) = kurtosis(sig5);
            ftrs(i) = (ftrs(i)-ftrmean(29))/ftrstd(29);
        case 30
            ftrs(i) = skewness(sig5);
            ftrs(i) = (ftrs(i)-ftrmean(30))/ftrstd(30);
        case 31
            ftrs(i) = mean(calcFFT(sig6,Fs,25,30));
            ftrs(i) = (ftrs(i)-ftrmean(31))/ftrstd(31);
        case 32
            ftrs(i) = std(calcFFT(sig6,Fs,25,30),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(32))/ftrstd(32);
        case 33
            ftrs(i) = max(abs(sig6));
            ftrs(i) = (ftrs(i)-ftrmean(33))/ftrstd(33);
        case 34
            ftrs(i) = std(sig6);
            ftrs(i) = (ftrs(i)-ftrmean(34))/ftrstd(34);
        case 35
            ftrs(i) = kurtosis(sig6);
            ftrs(i) = (ftrs(i)-ftrmean(35))/ftrstd(35);
        case 36
            ftrs(i) = skewness(sig6);
            ftrs(i) = (ftrs(i)-ftrmean(36))/ftrstd(36);
        case 37
            ftrs(i) = mean(calcFFT(sig7,Fs,30,45));
            ftrs(i) = (ftrs(i)-ftrmean(37))/ftrstd(37);
        case 38
            ftrs(i) = std(calcFFT(sig7,Fs,30,45),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(38))/ftrstd(38);
        case 39
            ftrs(i) = max(abs(sig7));
            ftrs(i) = (ftrs(i)-ftrmean(39))/ftrstd(39);
        case 40
            ftrs(i) = std(sig7);
            ftrs(i) = (ftrs(i)-ftrmean(40))/ftrstd(40);
        case 41
            ftrs(i) = kurtosis(sig7);
            ftrs(i) = (ftrs(i)-ftrmean(41))/ftrstd(41);
        case 42
            ftrs(i) = skewness(sig7);
            ftrs(i) = (ftrs(i)-ftrmean(42))/ftrstd(42);
        case 43
            ftrs(i) = mean(calcFFT(sig8,Fs,65,80));
            ftrs(i) = (ftrs(i)-ftrmean(43))/ftrstd(43);
        case 44
            ftrs(i) = std(calcFFT(sig8,Fs,65,80),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(44))/ftrstd(44);
        case 45
            ftrs(i) = max(abs(sig8));
            ftrs(i) = (ftrs(i)-ftrmean(45))/ftrstd(45);
        case 46
            ftrs(i) = std(sig8);
            ftrs(i) = (ftrs(i)-ftrmean(46))/ftrstd(46);
        case 47
            ftrs(i) = kurtosis(sig8);
            ftrs(i) = (ftrs(i)-ftrmean(47))/ftrstd(47);
        case 48
            ftrs(i) = skewness(sig8);
            ftrs(i) = (ftrs(i)-ftrmean(48))/ftrstd(48);
        case 49
            ftrs(i) = mean(calcFFT(sig9,Fs,80,100));
            ftrs(i) = (ftrs(i)-ftrmean(49))/ftrstd(49);
        case 50
            ftrs(i) = std(calcFFT(sig9,Fs,80,100),[],1);
            ftrs(i) = (ftrs(i)-ftrmean(50))/ftrstd(50);
        case 51
            ftrs(i) = max(abs(sig9));
            ftrs(i) = (ftrs(i)-ftrmean(51))/ftrstd(51);
        case 52
            ftrs(i) = std(sig9);
            ftrs(i) = (ftrs(i)-ftrmean(52))/ftrstd(52);
        case 53
            ftrs(i) = kurtosis(sig9);
            ftrs(i) = (ftrs(i)-ftrmean(53))/ftrstd(53);
        case 54
            ftrs(i) = skewness(sig9);
            ftrs(i) = (ftrs(i)-ftrmean(54))/ftrstd(54);
        otherwise
            warning('extractSelectedFtrs.m: Selected feature does not appear in the list of possible features');
    end
end
