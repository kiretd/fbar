clear all
load trainingdata4

DATA = IXDATA.raw;
% LABELS.cues = IXDATA.m_struct.i_names;
% LABELS.cuetime = IXDATA.m_struct.i_times;


for c = 1:4
    [IXDATA,Y] = traininglabels4(c);
    LABELS.artifactsegments(c,:) = Y(1,:);
    LABELS.lowfrequency(c,:) = Y(2,:);
    LABELS.emg(c,:) = double(Y(3,:)|Y(4,:));
end
