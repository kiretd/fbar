Filter-Bank Artifact Detection for EEG

This is a MATLAB toolbox for single-channel EEG artifact detection in 
offline or real-time settings. 

This is currently Version 0.9 - Pre-release version. I am still planning on 
updating code, cleaning things up, and adding some functionaity. 


If you use this toolbox for your research, please cite the publication below.
 
http://www.sciencedirect.com/science/article/pii/S1746809417301180
Dhindsa, Kiret. "Filter-Bank Artifact Rejection: High performance real-time 
single-channel artifact detection for EEG." Biomedical Signal Processing 
and Control 38 (2017): 224-235.