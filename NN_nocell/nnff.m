function nn = nnff(nn, x, y)
%NNFF performs a feedforward pass
% nn = nnff(nn, x, y) returns an neural network structure with updated
% layer activations, error and loss (nn.a, nn.e and nn.L)

    n = nn.n;
    m = size(x, 1);
    
    x = [ones(m,1) x];
    nn.a1 = x;

    %feedforward pass
    for i = 2 : n-1
        switch nn.activation_function 
            case 'sigm'
                % Calculate the unit's outputs (including the bias term)
                if i == 2
                    nn.a2 = sigm(nn.a1 * nn.W1');
                elseif i == 3
                    nn.a3 = sigm(nn.a2*nn.W2');
                end
            case 'tanh_opt'
                if i == 2
                    nn.a2 = tanh_opt(nn.a1 * nn.W1');
                elseif i == 3
                    nn.a3 = tanh_opt(nn.a2 * nn.W2');
                end
        end
        
        %dropout
        if(nn.dropoutFraction > 0)
            if(nn.testing)
%                 nn.a{i} = nn.a{i}.*(1 - nn.dropoutFraction);
            else
%                 nn.dropOutMask{i} = (rand(size(nn.a{i}))>nn.dropoutFraction);
%                 nn.a{i} = nn.a{i}.*nn.dropOutMask{i};
            end
        end
        
        %calculate running exponential activations for use with sparsity
        if(nn.nonSparsityPenalty>0)
%             nn.p{i} = 0.99 * nn.p{i} + 0.01 * mean(nn.a{i}, 1);
        end
        
        %Add the bias term
        if i == 2
            nn.a2 = [ones(m,1) nn.a2];
        elseif i == 3
            nn.a3 = [ones(m,1),nn.a3];
        end
    end
    switch nn.output 
        case 'sigm'
%             nn.a3 = sigm(nn.a{n - 1} * nn.W{n - 1}');
        case 'linear'
%             nn.a{n} = nn.a{n - 1} * nn.W{n - 1}';
        case 'softmax'
            nn.a3 = nn.a2 * nn.W2';
            nn.a3 = exp(bsxfun(@minus, nn.a3, max(nn.a3,[],2)));
            nn.a3 = bsxfun(@rdivide, nn.a3, sum(nn.a3, 2)); 
    end

    %error and loss
    nn.e = y - nn.a3;
    
    switch nn.output
        case {'sigm', 'linear'}
            nn.L = 1/2 * sum(sum(nn.e .^ 2)) / m; 
        case 'softmax'
            nn.L = -sum(sum(y .* log(nn.a3))) / m;
    end
end
