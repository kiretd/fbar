function nn = nnapplygrads(nn)
%NNAPPLYGRADS updates weights and biases with calculated gradients
% nn = nnapplygrads(nn) returns an neural network structure with updated
% weights and biases
    
    for i = 1 : (nn.n - 1)
        if(nn.weightPenaltyL2>0)
            if i == 1
                dW = nn.dW1 + nn.weightPenaltyL2 * nn.W1;
            elseif i == 2
                dW = nn.dW2 + nn.weightPenaltyL2 * nn.W2;
            end
        else
            if i == 1
                dW = nn.dW1;
            elseif i == 2
                dW = nn.dW2;
            end
        end
        
        dW = nn.learningRate * dW;
        
        if(nn.momentum>0)
            if i == 1
                nn.vW1 = nn.momentum*nn.vW1 + dW;
                dW = nn.vW1;
            elseif i == 2
                nn.vW2 = nn.momentum*nn.vW2 + dW;
                dW = nn.vW2;
            end
        end
        if i == 1
            nn.W1 = nn.W1 - dW;
        elseif i == 2
            nn.W2 = nn.W2 - dW;
        end
    end
end
