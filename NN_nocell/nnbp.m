function nn = nnbp(nn)
%NNBP performs backpropagation
% nn = nnbp(nn) returns an neural network structure with updated weights 
    
    n = nn.n;
    sparsityError = 0;
    switch nn.output
        case 'sigm'
            d3 = - nn.e .* (nn.a3 .* (1 - nn.a3));
        case {'softmax','linear'}
            d3 = - nn.e;
    end
    for i = (n - 1) : -1 : 2
        % Derivative of the activation function
        switch nn.activation_function 
            case 'sigm'
                d_act = nn.a2 .* (1 - nn.a2);
            case 'tanh_opt'
                d_act = 1.7159 * 2/3 * (1 - 1/(1.7159)^2 * nn.a2.^2);
        end
        
        if(nn.nonSparsityPenalty>0)
            pi = repmat(nn.p{i}, size(nn.a2, 1), 1);
            sparsityError = [zeros(size(nn.a2,1),1) nn.nonSparsityPenalty * (-nn.sparsityTarget ./ pi + (1 - nn.sparsityTarget) ./ (1 - pi))];
        end
        
        % Backpropagate first derivatives
        if i+1==n % in this case in d{n} there is not the bias term to be removed             
            d2 = (d3 * nn.W2 + sparsityError) .* d_act; % Bishop (5.56)
        else % in this case in d{i} the bias term has to be removed
            d1 = (d2(:,2:end) * nn.W1 + sparsityError) .* d_act;
        end
        
        if(nn.dropoutFraction>0)
            d2 = d2 .* [ones(size(d2,1),1) nn.dropOutMask2];
        end

    end

    for i = 1 : (n - 1)
        if i+1==n
            nn.dW2 = (d3' * nn.a2) / size(d3, 1);
        else
            nn.dW1 = (d2(:,2:end)' * nn.a1) / size(d2, 1);      
        end
    end
end
