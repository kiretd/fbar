% function load and format the data
function [X, true_y] = parseData(dataSetFile, trueLabelFile, opt)

% Loading the Data:
load(dataSetFile);
load(trueLabelFile);

%Formatting the data:
cnt = 0.1*double(cnt); % the continuous EEG signal in uV
sampling_rate = nfo.fs; 
% y is the vector of the target class. (0 , 1, NaN -- for comp purpose)
% need to normalize the eegdata signal by subtracting the mean.
% note that this is not done here becuase the competition data is already
% normalized

% E: raw eeg data (I think this is one trial)
% N: number of channels
% T: number of samples per channel

E = cnt'; % raw eeg data. (NxT matrix)  
[N, T] = size(E);

numTrials = length(mrk.y); % Number of Trials
windowMax = opt.winMax;
cutoff = opt.winSkip;
windowLength = windowMax-cutoff;

X = zeros(N,windowLength,numTrials);
for t = 1:numTrials
    startind = mrk.pos(t)+cutoff;
    endind = mrk.pos(t) + windowMax-1;
    X(:,:,t) = E(:,startind:endind);
end

% class1 = X(:,:,true_y==1);
% class2 = X(:,:,true_y==2);
