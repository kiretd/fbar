clear all; close all;
% rmpath('/1/local/matlab/r2013a/toolbox/stats/stats');
% addpath(genpath('/home/dhindsj/Dropbox/EEG/Current BCI/Classifiers/LIBSVM'))


%% Options
% Plot Options
opt.plot.raw = 1;
opt.plot.csp_components = 1;
opt.plot.csp_reconstruct = 1;
opt.plot.mrmr = 0;

% Data Options
opt.data.winSkip = 0;
opt.data.winMax = 220;

% Feature Selection Options
opt.ftr_sel.method = 'none'; % 'MRMR', 'AP', 'none'
opt.ap.version = ''; % 'bands', 'paper', 'pairs', 'centroids'

% Classification Options
opt.class.type = 'svm-matlab'; % 'svm', 'svm-matlab'
opt.class.param = '-s 0 -t 0 -q';
opt.class.runs = 1;

% Filter Options
opt.filter.type = 'butter'; % 'butter', 'cheby2'
opt.filter.order = 40;
opt.filter.atten = 100;
opt.filter.global_low = 0;
opt.filter.global_high = 40;
opt.filter.step = 2;
opt.filter.width = 40;
opt.filter.sr = 220;

% CSP Options
opt.csp.mode = 'our'; % 'our', 'ourcat', 'bcilab'
opt.csp.m = 2;

% Cheatcodes
opt.cheats.csp = 0;
opt.cheats.ap = 0;

if strcmp(opt.class.type,'svm-matlab')
    addpath('/1/local/matlab/r2013a/toolbox/stats/stats');
    rmpath(genpath('/home/dhindsj/Dropbox/EEG/Current BCI/Classifiers/LIBSVM'))
end


%% Load Data
[~,hostname] = system('hostname');
if strcmp(hostname(1:end-1),'hopper');
    fpath = '/home/dhindsj/Dropbox/InteraXon/Data/CLEAN APP DATA FEB 7/Artifact data/';
elseif strcmp(hostname(1:end-1),'AIm')
    fpath = 'C:\Users\Kiret\Dropbox\InteraXon\Data\CLEAN APP DATA FEB 7\Artifact data\';
elseif strcmp(hostname(1:end-1),'Kiret-Desktop')
    fpath = 'E:\Dropbox\InteraXon\Data\CLEAN APP DATA FEB 7\Artifact data\';
end
F = dir([fpath,'*.mat']);
nSub = length(F);

% Obtain Epoched Raw Data
trainfeats = cell(nSub,1);
trainlabels = trainfeats;
subinds = ones(nSub,2);
for ss = 1:nSub
    [trainfeats{ss},trainlabels{ss}] = parseMuseData(F(ss).name,opt.data);
    if ss > 1
        subinds(ss,1) = subinds(ss-1,2) + 1;
    end
    subinds(ss,2) = subinds(ss,1) + length(trainlabels{ss}) -1;
end
X = cat(3,trainfeats{:});
Y = horzcat(trainlabels{:});
[nChans,nSamps,nTrials] = size(X);


%% Main Loop
ACC = zeros(nSub,opt.class.runs);

for s = 1:nSub
    fprintf('SUBJECT: %g\n',s);
    subidx = [1:s-1,s+1:nSub];
    indtest = subinds(s,1):subinds(s,2);
    indtrain = [1:subinds(s,1)-1,subinds(s,2)+1:nTrials];
    
    for run = 1:opt.class.runs;
        [Xcsp,F,Fmat,W] = getSWDCSPfeatures(X,Y,indtrain,opt);
        
        % Plot Raw Signal
        if opt.plot.raw
            Xplot = reshape(X,nChans,nSamps*nTrials);
            figure;
            hraw = zeros(nChans,1);
            for c = 1:nChans
                hraw(c) = subplot(nChans,1,c);
                plot(Xplot(c,:));
            end
            linkaxes(hraw,'xy');
        end
        
        % Plot CSP components
        if opt.plot.csp_components
            Xcsp_plot = reshape(Xcsp,2*opt.csp.m,nSamps*nTrials,[]);
            for b = 1:size(Xcsp_plot,3)
                hcsp = zeros(opt.csp.m*2,1);
                figure;
                for c = 1:opt.csp.m*2
                    hcsp(c) = subplot(2*opt.csp.m,1,c);
                    plot(Xcsp_plot(c,:,b))
                end
                linkaxes(hcsp,'xy');
            end
        end
        
        % Plot CSP Reconstruction
        if opt.plot.csp_reconstruct
            Xcsp_cat = reshape(Xcsp,2*opt.csp.m,nSamps*nTrials,[]);
            for b = 1:size(W,3)
                Winv = inv(W(:,:,b));
                Xgood = Xcsp_cat(:,:,b);
                Xgood(1:2,:) = 0;
                Xrecon = Winv*Xgood;
                
                hcsp = zeros(opt.csp.m*2,1);
                figure;
                for c = 1:size(Xrecon,1)
                    hcsp(c) = subplot(2*opt.csp.m,1,c);
                    plot(Xrecon(c,:,b))
                end
                linkaxes(hcsp,'xy');
            end
        end
        
        % MRMR Feature Selection and Classification with SVM
        if strcmp(opt.ftr_sel.method,'MRMR')
            K = 2:2:size(F,1);
            acc = zeros(length(K),1);
            for k = 1:length(K)
                sel = mrmr_mid_d(F(:,indtrain)',Y(:,indtrain),K(k));
                Fsel = F(sel,:);
                
                [SVM] = classify_svm_current(Fsel(:,indtrain)',Y(indtrain)',Fsel(:,indtest)',Y(indtest)',0,opt.class.param);
                acc(k) = SVM.acc(1);
                if acc(k)>=max(acc)
                    fprintf('Best ACC: %g\n',acc(k))
                    bestSVM = SVM;
                end
            end
            ACC(s,run) = max(acc);
            
            if opt.plot.mrmr
                h = figure;
                plot(K,acc)
                xlabel('Num Features Selected with MRMR')
                ylabel('Percent Classification Accuracy')
                title('SWDCSP with MRMR and SVM, Full data for CSP')
                saveas(h,['Results_swdcsp_fullset_mrmr_svm_',subject],'jpg');
            end
            
        % Feature Selection with AP    
        elseif strcmp(opt.ftr_sel.method,'AP')
            if opt.cheats.ap
                indap = 1:280;
            else
                indap = indtrain;
            end
            
            if strcmp(opt.ap.version,'paper') % take bands from paper [1]
                idxbands{1} = [8,10,11,15,16];
                idxbands{2} = [1,4,6,9,10,14];
                idxbands{3} = [8,9,11,12,13,15];
                idxbands{4} = [5,6,14,15,16];
                idxbands{5} = [2,8,9,10,11,14,15];
                Fsel = Fmat(:,idxbands{s},:);
                Fsel = reshape(Fsel,2*opt.csp.m*length(unique(idxbands{s})),nTrials);
                
            elseif strcmp(opt.ap.version,'centroids') % take centroid features only
                [Sim,idx] = affinity_propagation(F(:,indap));
                Fsel = F(unique(idx),:);
                
            elseif strcmp(opt.ap.version,'bands') % take bands containing centroid features
                [Sim,idx] = affinity_propagation(F(:,indap));
                idxbands{s} = unique(ceil(unique(idx)/4));
                Fsel = Fmat(:,idxbands{s},:);
                Fsel = reshape(Fsel,4*length(unique(idxbands{s})),nTrials);
                fprintf('Selected Bands: %g\n',idxbands{s});
                
            elseif strcmp(opt.ap.version,'pairs') % take centroid features and corresponding pairs
                [Sim,idx] = affinity_propagation(F(:,indap));
                pairs = (mod(unique(idx),4)==1).*(unique(idx)+3) + (mod(unique(idx),4)==2).*(unique(idx)+1) + (mod(unique(idx),4)==3).*(unique(idx)-1) + (mod(unique(idx),4)==0).*(unique(idx)-3);
                idxpairs = cat(1,unique(idx),pairs);
                Fsel = F(idxpairs,:);
                
            elseif strcmp(opt.ap.version,'2d') % use 2d affinity propagation
                [Sim,idx] = affinity_propagation(Fmat(:,:,indap));
                Fsel = Fmat(:,unique(idx),:);
                Fsel = reshape(Fsel,2*opt.csp.m*length(unique(idx)),nTrials);
                fprintf('Selected Bands: %g\n',unique(idx));
            end
            
        elseif strcmp(opt.ftr_sel.method,'none')
            Fsel = F;
        end
        
        % Classification
        if strcmp(opt.class.type,'svm-cv')
            CV{s} = libsvm_paramSearch(Fsel(:,indtrain)',Y(indtrain)',10,opt.class.param);
            param2 = [' -c ',num2str(CV{s}.c),' -g ',num2str(CV{s}.g)];
            SVM{s} = classify_svm_current(Fsel(:,indtrain)',Y(indtrain)',Fsel(:,indtest)',Y(indtest)',0,[opt.class.param,param2]);
            ACC(s,run) = SVM{s}.acc(1);
            %                     break;
        elseif strcmp(opt.class.type,'svm')
            [SVM{s}] = classify_svm_current(Fsel(:,indtrain)',Y(indtrain)',Fsel(:,indtest)',Y(indtest)',0,opt.class.param);
            ACC(s,run) = SVM{s}.acc(1);
            
        elseif strcmp(opt.class.type,'svm-matlab')
            SVM{s} = svmtrain(Fsel(:,indtrain)',Y(indtrain),'options',statset('MaxIter',150000));
            Yhat = svmclassify(SVM{s},Fsel(:,indtest)');
            ACC(s,run) = mean(Yhat'==Y(indtest))*100;
            fprintf('SVM accuracy for Subject %g: %g\n',s,ACC(s,run));
        end
    end
    fprintf('MEAN ACCURACY FOR SUBJECT %g FOR RUN %g: %g (%g)\n',s,run,mean(ACC(s,:)),std(ACC(s,:)));
end


%% Save Results
fpath = '/home/dhindsj/Dropbox/EEG/BCI Comp/Code/csp/SWD CSP/Results/';
fname = 'MuseArtifacts_SWDCSP_Results';
saveResults2Text(fpath,fname,{'1','2','3','4','5'},ACC,opt)






