function [filtered_sig] = frequency_filtering(sig, lowFreq, highFreq, fs, order)

%     [b,a] = butter(order, [lowFreq highFreq]/(fs/2), 'bandpass');
    [b,a] = cheby2(6,80,[lowFreq,highFreq]/(fs/2),'bandpass');
    filtered_sig = filter(b,a,sig');

end 








