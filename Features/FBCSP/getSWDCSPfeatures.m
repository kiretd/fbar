function [Xcsp,F,cspftrs,Psel] = getSWDCSPfeatures(X,Y,indtrain,opt)

% Author: Kiret
% -----------------------------------------------------------------------
% Description: 
% This function computes the csp feature vectors for each sub-band of 
% of filtered EEG. 
% -----------------------------------------------------------------------
% Dependancies: 
% csp_base
% projection_matrix.m
% -----------------------------------------------------------------------
% Inputs: 
% 
% 
% -----------------------------------------------------------------------
% Outputs: 
% 
% 
% -----------------------------------------------------------------------
% References:
% [1]   J. H. a. G. W. Gufei SUN, "A Novel Frequency Band Selection Method for 
%       Common Spatial," IEEE World Congress on Computational Intelligence, 
%       pp. 1- 6, 2010. 
%------------------------------------------------------------------------

% Indexing
[nChans,nSamps,nTrials] = size(X);
nBands = ((opt.features.cspband(2)-(opt.features.cspwidth-opt.features.cspstep))...
            - opt.features.cspband(1))/opt.features.cspstep;
sel = [1:opt.features.cspcomps,nChans-opt.features.cspcomps+1:nChans];

% Init outputs
P = zeros(2*opt.features.cspcomps,nChans,nBands);
cspftrs = zeros(2*opt.features.cspcomps,nBands,nTrials);
Xcsp = zeros(2*opt.features.cspcomps,nSamps,nTrials,nBands);

% Initial frequency band
low = opt.features.cspband(1);
high = low + opt.features.cspwidth;

% Normalize all trials
Xnorm = zeros(size(X));
for t = 1: nTrials
    Xnorm(:,:,t) = bsxfun(@minus,X(:,:,t),mean(X(:,:,t),2));
end

for band = 1:nBands
    % Bandpass filter normalized trials
    if strcmp(opt.filter.type,'butter')
        [b,a] = butter(opt.filter.order,[low,high]/(opt.filter.sr/2));
    elseif strcmp(opt.filter.type,'cheby2')
        [b,a] = cheby2(opt.filter.order,opt.filter.atten,[low,high]/(opt.filter.sr/2));
    elseif strcmp(opt.filter.type,'fir')
        b = fir1(opt.filter.order,[low,high]/(opt.filter.sr/2));
        a = 1;
    end
    
    Xfilt = zeros(size(Xnorm));
    for tr = 1:nTrials
        Xfilt(:,:,tr) = filter(b,a,Xnorm(:,:,tr)')';
    end
    
    % CSP projection matrix
     P(:,:,band) = csp_base(Xfilt(:,:,indtrain),Y(indtrain),opt);
    
    % Project Signals
    Xproj = zeros(size(P,1),nSamps,nTrials);
    for tr = 1:nTrials
        Xproj(:,:,tr) = P(:,:,band)*Xfilt(:,:,tr);
    end
    
    % Compute Variances
    Xvars = zeros(size(P,1),nTrials);
    for tr = 1: nTrials
        Xvars(:,tr) = var(Xproj(:,:,tr),[],2);
    end
    
    % Compute CSP features
    for tr = 1: nTrials
        cspftrs(:,band,tr) = log10(Xvars(:,tr)./sum(Xvars(:,tr)));
    end
    
    % Update frequency band and Output args
    low = low + opt.features.cspstep;
    high = high + opt.features.cspstep;
    Xcsp(:,:,:,band) = Xproj;
end

% Output
% Psel = P(sel,:,:);
Psel = P;
F = reshape(cspftrs,2*opt.features.cspcomps*nBands,nTrials);




