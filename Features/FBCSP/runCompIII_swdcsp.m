clear all; close all;
rmpath('/1/local/matlab/r2013a/toolbox/stats/stats');
addpath(genpath('/home/dhindsj/Dropbox/EEG/Current BCI/Classifiers/LIBSVM'))
S{1} = 'aa'; S{2} = 'al'; S{3} = 'av'; S{4} = 'aw'; S{5} = 'ay';

%% Options
% Data Options
opt.data.winSkip = 50;
opt.data.winMax = 250;

% Feature Selection Options
opt.ftr_sel.method = 'AP'; % 'MRMR'
opt.ap.version = 'paper'; % 'bands', 'paper', 'pairs', 'centroids'

% Classification Options
opt.class.type = 'svm-matlab'; % 'svm', 'svm-matlab'
opt.class.param = '-s 0 -t 0 -q';
opt.class.cv = 10;
opt.class.runs = 10;

% Filter Options
opt.filter.type = 'butter'; % 'butter', 'cheby2'
opt.filter.order = 4;
opt.filter.atten = 100;
opt.filter.global_low = 4;
opt.filter.global_high = 40;
opt.filter.step = 2;
opt.filter.width = 4;
opt.filter.sr = 100;

% CSP Options
opt.csp.mode = 'ourcat'; % 'our', 'ourcat', 'bcilab'
opt.csp.m = 3;

% Cheatcodes
opt.cheats.csp = 0;
opt.cheats.ap = 0;

if strcmp(opt.class.type,'svm-matlab')
    addpath('/1/local/matlab/r2013a/toolbox/stats/stats');
    rmpath(genpath('/home/dhindsj/Dropbox/EEG/Current BCI/Classifiers/LIBSVM'))
end


%%
ACC = zeros(length(S),opt.class.cv,opt.class.runs);

for s = 1:length(S)
    fprintf('SUBJECT: %s\n',S{s});
    subject = S{s};
    
    % Obtain Epoched Raw Data
    [X, Y] = parseData(['data_set_IVa_',subject], ['true_labels_',subject],opt.data);
    nTrials = length(Y);
    
    for run = 1:opt.class.runs;
        %     indtrain = 1:sum(~isnan(mrk.y));
        %     indtest = max(indtrain)+1:nTrials;
        cvidx = crossvalind('kfold',nTrials,opt.class.cv);
        
        % Begin Cross-Validation
        for cv = 1:opt.class.cv
            %     for cv = 1:10
            indtrain = find(cvidx~=cv);
            indtest = find(cvidx==cv);
            
            if ~(opt.cheats.csp && (cv>1||run>1))
                tic;
                [F,Fmat,W] = getSWDCSPfeatures(X,Y,indtrain,opt);
                toc;
            end
            
            if strcmp(opt.ftr_sel.method,'MRMR')
                K = 2:2:size(F,1);
                acc = zeros(length(K),1);
                for k = 1:length(K)
                    %                 sel = mrmr_mid_d(F(:,indtrain)',Y(:,indtrain),K(k));
                    sel = mrmr_mid_d(F(:,indtrain)',Y(:,indtrain),K(k));
                    Fsel = F(sel,:);
                    
                    [SVM] = classify_svm_current(Fsel(:,indtrain)',Y(indtrain)',Fsel(:,indtest)',Y(indtest)',0,opt.class.param);
                    acc(k) = SVM.acc(1);
                    if acc(k)>=max(acc)
                        fprintf('Best ACC: %g\n',acc(k))
                        bestSVM = SVM;
                    end
                end
                ACC(s,cv,run) = max(acc);
                
                h = figure;
                plot(K,acc)
                xlabel('Num Features Selected with MRMR')
                ylabel('Percent Classification Accuracy')
                title('SWDCSP with MRMR and SVM, Full data for CSP')
                saveas(h,['Results_swdcsp_fullset_mrmr_svm_',subject],'jpg');
                
            elseif strcmp(opt.ftr_sel.method,'AP')
                if opt.cheats.ap
                    indap = 1:280;
                else
                    indap = indtrain;
                end
                
                if strcmp(opt.ap.version,'paper') % take bands from paper [1]
                    idxbands{1} = [8,10,11,15,16];
                    idxbands{2} = [1,4,6,9,10,14];
                    idxbands{3} = [8,9,11,12,13,15];
                    idxbands{4} = [5,6,14,15,16];
                    idxbands{5} = [2,8,9,10,11,14,15];
                    Fsel = Fmat(:,idxbands{s},:);
                    Fsel = reshape(Fsel,2*opt.csp.m*length(unique(idxbands{s})),nTrials);
                    
                elseif strcmp(opt.ap.version,'centroids') % take centroid features only
                    [Sim,idx] = affinity_propagation(F(:,indap));
                    Fsel = F(unique(idx),:);
                    
                elseif strcmp(opt.ap.version,'bands') % take bands containing centroid features
                    [Sim,idx] = affinity_propagation(F(:,indap));
                    idxbands{s} = unique(ceil(unique(idx)/4));
                    Fsel = Fmat(:,idxbands{s},:);
                    Fsel = reshape(Fsel,4*length(unique(idxbands{s})),nTrials);
                    fprintf('Selected Bands: %g\n',idxbands{s});
                    
                elseif strcmp(opt.ap.version,'pairs') % take centroid features and corresponding pairs
                    [Sim,idx] = affinity_propagation(F(:,indap));
                    pairs = (mod(unique(idx),4)==1).*(unique(idx)+3) + (mod(unique(idx),4)==2).*(unique(idx)+1) + (mod(unique(idx),4)==3).*(unique(idx)-1) + (mod(unique(idx),4)==0).*(unique(idx)-3);
                    idxpairs = cat(1,unique(idx),pairs);
                    Fsel = F(idxpairs,:);
                    
                elseif strcmp(opt.ap.version,'2d') % use 2d affinity propagation
                    [Sim,idx] = affinity_propagation(Fmat(:,:,indap));
                    Fsel = Fmat(:,unique(idx),:);
                    Fsel = reshape(Fsel,2*opt.csp.m*length(unique(idx)),nTrials);
                    fprintf('Selected Bands: %g\n',unique(idx));
                end
                
                % Classifier
                if strcmp(opt.class.type,'svm-cv')
                    CV{s} = libsvm_paramSearch(Fsel(:,indtrain)',Y(indtrain)',opt.class.cv,opt.class.param);
                    param2 = [' -c ',num2str(CV{s}.c),' -g ',num2str(CV{s}.g)];
                    SVM{s} = classify_svm_current(Fsel(:,indtrain)',Y(indtrain)',Fsel(:,indtest)',Y(indtest)',0,[opt.class.param,param2]);
                    ACC(s,cv,run) = SVM{s}.acc(1);
%                     break;
                elseif strcmp(opt.class.type,'svm')
                    [SVM{s}] = classify_svm_current(Fsel(:,indtrain)',Y(indtrain)',Fsel(:,indtest)',Y(indtest)',0,opt.class.param);
                    ACC(s,cv,run) = SVM{s}.acc(1);
                    
                elseif strcmp(opt.class.type,'svm-matlab')
                    SVM{s} = svmtrain(Fsel(:,indtrain)',Y(indtrain));
                    Yhat = svmclassify(SVM{s},Fsel(:,indtest)');
                    ACC(s,cv,run) = mean(Yhat'==Y(indtest))*100;
                    fprintf('SVM accuracy for Subject %s: %g\n',S{s},ACC(s,cv,run));
                end
            end
        end
        fprintf('MEAN ACCURACY FOR SUBJECT %s FOR RUN %g: %g (%g)\n',S{s},run,mean(ACC(s,:,run)),std(ACC(s,:,run)));
    end
    acc = reshape(ACC(s,:,:),1,opt.class.runs*opt.class.cv);
    fprintf('MEAN ACCURACY FOR SUBJECT %s OVER ALL RUNS: %g (%g)\n',S{s},mean(acc),std(acc));
end
acc = reshape(ACC,length(S),opt.class.runs*opt.class.cv);
%% Save Results
fpath = '/home/dhindsj/Dropbox/EEG/BCI Comp/Code/csp/SWD CSP/Results/';
fname = 'ReplacateResults_10x10cv';
saveResults2Text(fpath,fname,S,acc,opt)






