        function defaults = preprocessing_defaults(self)
            % define the default pre-processing parameters of this paradigm
            defaults = {'FIRFilter',{'Frequencies',[6 8 28 32],'Type','minimum-phase'}, 'EpochExtraction',[0.5 3.5], 'Resampling',100};
        end
                
        function model = feature_adapt(self,varargin)
            % adapt a feature representation using the CSP algorithm
            arg_define(varargin, ...
                arg_norep('signal'), ...
                arg({'patterns','PatternPairs'},3,[],'Number of CSP patterns (times two).','cat','Feature Extraction','type','expression','shape','row'),...
                arg({'shrinkage_cov','ShrinkageCovariance'},false,[],'Shrinkage covariance estimator. Whether to use shrinkage to estimate the covariance matrices.'));
            
            if signal.nbchan < patterns
                error('CSP requires at least as many channels as you request output patterns. Please reduce the number of pattern pairs.'); end
            for k=1:2
                trials{k} = exp_eval(set_picktrials(signal,'rank',k));
                if shrinkage_cov
                    covar{k} = cov_shrink(reshape(trials{k}.data,size(trials{k}.data,1),[])');
                else
                    covar{k} = cov(reshape(trials{k}.data,size(trials{k}.data,1),[])');
                end
                covar{k}(~isfinite(covar{k})) = 0;
            end
            [V,D] = eig(covar{1},covar{1}+covar{2}); %#ok<NASGU>
            model.filters = V(:,[1:patterns end-patterns+1:end]);
            P = inv(V);
            model.patterns = P([1:patterns end-patterns+1:end],:);
            model.chanlocs = signal.chanlocs;
        end
        
        function features = feature_extract(self,signal,featuremodel)
            % extract log-variance features according to CSP
            features = zeros(size(signal.data,3),size(featuremodel.filters,2));
            for t=1:size(signal.data,3)
                features(t,:) = log(var(signal.data(:,:,t)'*featuremodel.filters)); end
        end
        
        function visualize_model(self,parent,featuremodel,predictivemodel) %#ok<*INUSD>
            % number of pairs, and index of pattern per subplot
            np = size(featuremodel.patterns,1)/2; idx = [1:np 2*np:-1:np+1];
            % for each CSP pattern...
            for p=1:np*2
                subplot(2,np,p,'Parent',parent);
                topoplot(featuremodel.patterns(idx(p),:),featuremodel.chanlocs);
                title(['CSP Pattern ' num2str(idx(p))]);
            end
        end
        
        function layout = dialog_layout_defaults(self)
            % define the default configuration dialog layout 
            layout = {'SignalProcessing.Resampling.SamplingRate', 'SignalProcessing.FIRFilter.Frequencies', ...
                'SignalProcessing.FIRFilter.Type', 'SignalProcessing.EpochExtraction', '', ...
                'Prediction.FeatureExtraction.PatternPairs', '', 'Prediction.MachineLearning.Learner'};
        end
        
        function tf = needs_voting(self)
            % standard CSP requires voting to handle more than 2 classes
            tf = true; 
        end
       