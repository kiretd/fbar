function feats = realtime_sqiFtrs(dataBuffer,args,filt)
    
nchan = 4;
window = 220;

inds = returnInds(window,dataBuffer.ind,dataBuffer.size);

eegData = dataBuffer.buffer(inds,:);

eegData = bsxfun(@minus,eegData,mean(eegData,1));

if exist('filt','var')
    eegData = filter(filt.B,filt.A,eegData);
end

maxAmp = max(abs(eegData),[],1);
stdAmp = std(eegData,[],1);
kurtAmp = kurtosis(eegData,0,1);
skewAmp = skewness(eegData,0,1);

feats = [maxAmp;stdAmp;kurtAmp;skewAmp];
end