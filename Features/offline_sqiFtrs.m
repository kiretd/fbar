datafile = 'Kiret12_IXDATA';
load(datafile);

freqbands =  [1 48;
           0.5 8;
           8 13;
           13 18;
           18 25];

for i = 1:size(freqbands,2)
    IXDATA = filteredEegStats(IXDATA,freqbands(i,1),freqbands(i,2));
end