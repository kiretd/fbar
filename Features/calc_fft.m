function Xfft = calc_fft(X,Fs,f1,f2)

[N,Nchans] = size(X);
nfft = 2^nextpow2(N);
f = Fs/2*linspace(0,1,nfft/2+1);

H = repmat(hamming(N),1,Nchans);
% HP = sum(hamming(N)).^2;
psd = fft(H.*X,nfft)/N;
% psd = (fft(H.*X))/(N*HP);
    %     Xfft(:,:,i) = abs(10*log10(psd(sind:eind,:)));
Xfft = abs(psd(find(f>=f1,1):find(f<=f2,1,'last'),:).^2);
