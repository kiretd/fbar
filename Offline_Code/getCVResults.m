function Result = getCVResults(Model)

nT = length(Model);
[nS,nK] = size(Model{1});
nP = length(Model{1}(1,1).WinAcc);

Result = cell(nT,1);
for t = 1:nT
    Result{t}.WinAcc = zeros(nS,nK,nP);
    Result{t}.TSAcc = zeros(nS,nK,nP);
    Result{t}.AUC = zeros(nS,nK,nP);
    Result{t}.FNR = zeros(nS,nK,nP);
    Result{t}.FPR = zeros(nS,nK,nP);
    for s = 1:nS
        for k = 1:nK
            Result{t}.WinAcc(s,k,:) = Model{t}(s,k).WinAcc;
            Result{t}.TSAcc(s,k,:) = Model{t}(s,k).TSacc;
            Result{t}.AUC(s,k,:) = Model{t}(s,k).AUC;
            for p = 1:nP
                if ~isnan(Model{t}(s,k).performance{1}(1,1))
                    Result{t}.FNR(s,k,p) = Model{t}(s,k).performance{1}(2,1);
                    Result{t}.FPR(s,k,p) = Model{t}(s,k).performance{1}(2,2);
                end
            end
        end
    end
end
            
    