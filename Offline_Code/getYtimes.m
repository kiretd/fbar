function Ytimes = getYtimes(Y,T,opt)
nWins = length(Y);
TStest = linspace(0,T,nWins);
Ts = floor((1:nWins)*opt.window.step+opt.window.winlen/2);

Ytimes = zeros(length(Xtest),1);
    Ytimes(1:TStest(1)) = Yhat(1)-1;
    for ep = 2:nWins-1
        Ytimes(TStest(ep-1):TStest(ep)) = Yhat(ep)-1;
        if (Yhat(ep)~=Yhat(ep-1))&&(Yhat(ep)~=Yhat(ep+1))
            Ytimes(TStest(ep-1):TStest(ep)) = Yhat(ep-1)-1;
        end
    end
    Ytimes(TStest(ep):end) = Yhat(nWins)-1;
    retained = mean(Ytimes==0);