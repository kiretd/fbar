function deleteFtrs(DATASETS,type)

nDS = length(DATASETS);
[f,~,~] = fileparts(which('artifacts1.mat'));
cd(f)

switch type
    case 'testftrs'
        for ds = 1:nDS
            origvars = who('-file',DATASETS{ds});
            load(DATASETS{ds})
            clear test*
            keepvars = origvars(~strncmpi(origvars,'test',4));
            save(DATASETS{ds},keepvars{:});
        end
        
    case 'trainftrs'
         for ds = 1:nDS
            origvars = who('-file',DATASETS{ds});
            load(DATASETS{ds})
            clear ftrs* labels* fft*
            rmidx = ~(strncmpi(origvars,'fft',3)|strncmpi(origvars,'ftrs',4)...
                |strncmpi(origvars,'labels',6));
            keepvars = origvars(rmidx);
            save(DATASETS{ds},keepvars{:});
        end
end
