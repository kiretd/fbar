%%%%%%%%%%%%%%%%%%%%%
% Notes:
% BUGS:
%   - model.testlabels not the same size as Yhat because training data is not
%   full dataset (other artifacts are removed from data for training)
%   - ftrStats is only taking the statistics from the last training set
%   - Test features in CV mode not normalized by total training stats
%   (possibly the reason classification fails on test set but works in
%   TestModel.m
%
% Failed Test Sets
%   sqiEog -- Set 10, Set 15
%
%
% Things to try
%   Probabilistic output with or without 'confidence labeling' of artifacts
%   Could try using original markers from data collection and define a
%   Gaussian window around it as confidence of artifact presence by sample
%
% Things to do
%   Shaded error region on feature plots
%%%%%%%%%%%%%%%%%%%%

% Data options
opt.data.chan = 1;
opt.data.realtime = 1;
for i = 1:22 %1:22
    opt.data.testset{i} = ['Kiret',num2str(i),'_IXDATA'];
end

% Plot options
opt.plot.raw = 0;
opt.plot.windows = 0;
opt.plot.features = 0;
opt.plot.selected_features = 0;
opt.plot.testVapp = 0;
opt.plot.testVlabels = 1;
opt.plot.barfft = 0;
opt.plot.TestAllModels = 0;
opt.plot.adjustlabels = 0;

% Window Options
opt.window.method = 'sliding';
opt.window.winlen = 1;
opt.window.step = 0.1;

% Feature Options
opt.features.K = [1,2,3,5,8]; %1:28
opt.features.type = 'sqiEog';
opt.features.cspcomps = 4;
opt.features.cspband = [1,21];
opt.features.cspstep = 2;
opt.features.cspwidth = 4;
opt.features.fft = 8:2:30;
opt.features.select = 'mrmr_q'; %'mrmr_q'

% Classification Options
opt.classify.class = 2; % 2 = EOG, 3 = EMG
opt.classify.classifier = 'svm'; % 'svm','mlp','lda','qda'
opt.classify.crossval = 1; % crossval over training data (1) or build full model (0)
opt.classify.labelcutoff = 0.2; %0:0.1:0.5;

opt.classify.da.prior = 0;

opt.classify.svm.kernel = 'rbf'; % 'linear','quadratic','polynomial','rbf','mlp','@kfun'

opt.classify.mlp.hidden = 50;
opt.classify.mlp.learningrate = 0.001;
opt.classify.mlp.numepochs  = 75;


% Results Analysis Options
% opt.results.nf = Imagery;
% opt.results.session = 3;
% opt.results.topF = 1;

%% Run Algorithm and Compile Results
% artifacts5 is all bad
DATASETS = {'artifacts1','artifacts2','artifacts3','artifacts4','artifacts5',...
    'artifacts6','artifacts7','artifacts8','artifacts9','artifacts10','artifacts11','artifacts12'};
L = opt.classify.labelcutoff;

nD = length(DATASETS);
nK = length(opt.features.K);
nL = length(L);

R.err = zeros(nD,nK,nL);
R.cte = zeros(nD,nK,nL);
R.auc = zeros(nD,nK,nL);
R.per = zeros(4,nD,nK,nL);

Test = cell(length(L),1);
for ll = 1:nL
% for ll = 4;
    fprintf('---------Label Cutoff: %g----------\n',L(ll));
    opt.classify.labelcutoff = L(ll);
    [model,FTRS,LABELS,TestResult] = TrainModel(DATASETS,opt);
    for d = 1:nD
        TestResult{d}.K = opt.features.K;
        R.err(d,:,ll) = TestResult{d}.WinErr;
        R.cte(d,:,ll) = TestResult{d}.CorrectedTimesErr;
        R.auc(d,:,ll) = TestResult{d}.AUC;
        for k = 1:nK
            if isfield(TestResult{d},'performance')&&~isempty(TestResult{d}.performance{k})
%                 R.per(:,d,k,ll) = TestResult{d}.performance{k}(1,:);
            else
                if length(unique(TestResult{d}.Yhatcv{k})) > 1
                    [~,~,~,per] = confusion(targtovec(TestResult{d}.Yhatcv{k})',...
                        targtovec(TestResult{d}.testlabels{k})');
                    R.per(:,d,k,ll) = per(1,:);
                end
            end
        end
    end
    Test{ll} = TestResult;
end



% save EOGmodels



