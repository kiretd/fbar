function [lengths] = getDataLengths(DATASETS)

nDS = length(DATASETS);
lengths = zeros(nDS,1);
for ds = 1:nDS
    load(DATASETS{ds},'IXDATA')
    lengths(ds) = length(IXDATA.raw.eeg.data);
end
lengths = lengths./220;
lengths = lengths./60;