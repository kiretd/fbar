function Result = doCV(DATASETS,opt)
%% HELP AND USAGE
% Gives the classification accuracy per cross-validation run for a dataset
% given the options set in opt. Default options will be set for any option
% not explicitly defined in opt.
%
% Written by Kiret Dhindsa
%   Last updated: 4/8/2015
%
% INPUTS:
%   Current Datasets:
%       DATASETS = {'artifacts_david','artifacts_saurabh','artifacts_kiret'};
%           Note: these datasets are manually given class labels
%   Options:
%       See below for options
%
% OUTPUTS:
%
%
%
% DATA OPTIONS
%  - opt.data.chan: integer or vector identifying EEG channels to use in
%    building and testing artifact models (default: 1, fbcsp will default
%    to all channels)
%  - opt.data.realtime: regular offline processing (0) or simulate
%    realtime (1)
%  - opt.data.testset: cell array of dataset names to use for
%    testing (default: {'Kiret1_IXDATA'})
%
% PLOT OPTIONS
%  (All default to 0, set to 1 to produce desired plots)
%  - opt.plot.raw: visually browse raw data
%  - opt.plot.timelock: plot averaged "ERP" (or time-locked signal for
%    non-ERP EEG data) in topological layout per class
%  - opt.plot.csp: plot the CSP component activity maps
%  - opt.plot.fft: plot the average FFT curve per class
%  - opt.plot.results: plot bar graphs of classification accuracy
%
% WINDOW OPTIONS
%  - opt.window.method: method for windowing the raw signals: (default: 'sliding')
%    'sliding': sliding time-window with winlen and step
%    'trials': epochs based on clean and artifactual segments
%  - opt.window.winlen: length of windows for feature extraction in seconds
%    (default: 1)
%  - opt.window.step: shift in windows for feature extraction in seconds
%    (default: 0.1)
%
% FILTERING OPTIONS
%  - opt.filter.bpfreq: frequency range for bandpass filter (default [2,40])
%
% FEATURE OPTIONS
%  - opt.features.K: vector of number of selected features to try
%    (e.g. 5:5:40)
%  - opt.features.type: type of feature extraction to use
%    ('realtimeEOG','sqitest','sqiAll','sqiEOG','sqiEMG','csp','fbcsp')
%  - opt.features.cspcomps: total number of components for csp, half of
%    total for fbcsp
%  - opt.features.cspband: global frequency range for fbcsp (e.g. [8,32])
%  - opt.features.cspstep: shift in frequency bands for fbcsp (e.g. 2)
%  - opt.features.cspwidth: width of frequency bands for fbcsp (e.g. 4)
%  - opt.features.fft: vector of fft bins for fft features (e.g. [8:2:30])
%  - opt.features.select: type of feature selection to use
%    ('all' (use all features),'mrmr_d','mrmr_q')
%
% CLASSIFICATION OPTIONS
%  - opt.classify.class: scaler or vector of classes over which to classify
%    1=Clean, 2=LowFreq, 3=EMG_Hard, 4=EMG_Light, 5=ECR 6=Blinks
%  - opt.classify.classifier: type of classifier to use
%    'mlp' = Neural Network, 'svm' = Support Vector Machine,
%    'lda' = Linear Discriminant Analysis
%  - opt.classify.crossval: 1 (cross-validation among training sets), or
%    0 (use all training data to build model and test on test sets)
%
% NEURAL NETWORK OPTIONS
%
%
% SUPPORT VECTOR MACHINE OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Warning and Error Messages for Incorrect Usage
if strcmp(opt.features.type,'fbcsp') && ~isvector(opt.data.chan)
    error('KD: FBCSP requires multiple channels')
end
if strcmp(opt.features.type,'fbcsp') && (length(opt.data.chan) < opt.features.cspcomps)
    error('KD: CSP cannot output more components than the number of input channels')
end

% Set figures to not come to foreground as they are generated
% set(0,'DefaultFigureVisible','off');

%% Set options and highest level indexing variables
% Handle channel options
if opt.classify.combinechans == 0
    opt.data.chan = opt.data.chan(1);
    if length(opt.data.chan)>1
        warning('\nOnly computing model for chan %d. Use doCV.m separately for each channel.\n',opt.data.chan(1));
    end
elseif opt.classify.combinechans == 1
    opt.data.chan = 1:4;
elseif opt.classify.combinechans == 2
    if opt.data.chan(1)==1 || opt.data.chan(1)==4
        opt.data.chan = [1,4];
    elseif opt.data.chan(1)==2 || opt.data.chan(1)==3
        opt.data.chan = 2:3;
    end
end

% Set Matlab SVM options
if strcmp(opt.classify.classifier,'svm')
    svm_opt = statset('MaxIter',10^6,'Display','off');
end

% Indexing Variables
nD = length(DATASETS);
K = opt.features.K;
nK = length(K);

% Training set options
if ~isfield(opt.classify,'ntrain')
    nTrain = nD-1;
elseif strcmpi(opt.classify.ntrain,'all')
    nTrain = 1:(nD-1);
elseif opt.classify.ntrain>(nD-1)
    nTrain = nD-1;
else 
    nTrain = opt.classify.ntrain;
end
nTR = length(nTrain);

% Create save file
savename = [datestr(clock),'_newResults'];
savename = strrep(savename,'-','_');
savename = strrep(savename,' ','_');
savename = strrep(savename,':','_');
save(savename,'opt')

%% Feature Extraction (saves features according to type and loads if they already exist)
[FTRS,LABS,~] = getTrainingData(DATASETS,opt);
nFtrs = size(FTRS{1},1);

%% Cross-validation loop
model = cell(nTR,1);
Result = cell(nTR,1);
for t = 1:nTR
    fprintf('-------- Number of Training Sets Being Used: T = %d of %d ---------\n',t,nTR)
    trainsets = nchoosek(1:nD,nTrain(t));
    nCombs = size(trainsets,1);
    for s = 1:nCombs
        fprintf('     Beginning CV loop for combination C = %g of %g... \n',s,nCombs);
        %% Get training sets, normalize features, and get ftr statistics
        % Combine training data
        Xtrain = cell2mat(FTRS(trainsets(s,:))');
        Ytrain = double(cell2mat(LABS(trainsets(s,:))'));
        
        % Normalize Features and get Stats for Training Set
        ftrs_max = max(Xtrain,[],2);
        ftrs_norm = zeros(size(Xtrain));
        ftrs_mean = mean(Xtrain,2);
        ftrs_std = zeros(nFtrs,1);
        for f = 1:nFtrs
            ftrs_norm(f,:) = bsxfun(@minus,Xtrain(f,:),ftrs_mean(f));
            ftrs_std(f) = std(ftrs_norm(f,:));
            ftrs_norm(f,:) = bsxfun(@rdivide,ftrs_norm(f,:),ftrs_std(f));
        end
        Xtrain = ftrs_norm;
        ftrStats.mean = ftrs_mean;
        ftrStats.std = ftrs_std;
        ftrStats.max = ftrs_max;
        
        %% Get feature selection order (order is consisten across number of selected features with MRMR)
        if strcmp(opt.features.select,'mrmr_d')
            selected = mrmr_mid_d(Xtrain',Ytrain',max(K));
        elseif strcmp(opt.features.select,'mrmr_q')
            selected = mrmr_miq_d(Xtrain',Ytrain',max(K));
        else
            selected = 1:nFtrs;
            K = nFtrs;
            nK = 1;
            opt.features.K = nFtrs;
        end
        
        %% Loop over each number of selected features in the feature set
        for k = 1:nK
            tic;
            fprintf('          Building model for number of features K = %g of %g... ',k,nK);
            % Get selected features
            Xsel = Xtrain(selected(1:K(k)),:);
            ftrStats.selected = selected(1:K(k));
            
            %% Train the model
            if strcmp(opt.classify.classifier,'mlp')
                % Neural Network
                factors = factor(length(Ytrain));
                [~,~,opts.batchsize] = find(factors(factors<=25),1,'last');
                if isempty(opts.batchsize); opts.batchsize = min(factors); end
                opts.numepochs = opt.classify.mlp.numepochs;
                opts.silent = 0;
                opts.plot = 0;
                model{t}(s,k) = nnsetup([size(Xsel,1),opt.classify.mlp.hidden,2]);
                model{t}(s,k).output               = 'softmax';                   %  use softmax output
                model{t}(s,k).learningRate         = opt.classify.mlp.learningrate;
                model{t}(s,k).momentum             = 0.9;
                model{t}(s,k).dropoutFraction      = 0.0;
                model{t}(s,k).scaling_learningRate = 1;
                % model.sparsityTarget       = 0.0;
                model{t}(s,k) = nntrain(model{t}(s,k), Xsel', targtovec(double(Ytrain)), opts);
                model{t}(s,k).ftrStats = ftrStats;
            elseif strcmp(opt.classify.classifier,'svm')
                % Support Vector Machine
                tempmodel = svmtrain(Xsel',Ytrain','options',svm_opt,'kernel_function',opt.classify.svm.kernel);
                model{t}(s,k).SupportVectors = tempmodel.SupportVectors;
                model{t}(s,k).Alpha = tempmodel.Alpha;
                model{t}(s,k).Bias = tempmodel.Bias;
                model{t}(s,k).KernelFunction = tempmodel.KernelFunction;
                model{t}(s,k).KernelFunctionArgs = tempmodel.KernelFunctionArgs;
                model{t}(s,k).GroupNames = tempmodel.GroupNames;
                model{t}(s,k).SupportVectorIndices = tempmodel.SupportVectorIndices;
                model{t}(s,k).ScaleData = tempmodel.ScaleData;
                model{t}(s,k).FigureHandles = tempmodel.FigureHandles;
            elseif strcmp(opt.classify.classifier,'lda')
                % Linear Discriminant Analysis
                [~,~,~,~,coeff] = classify(zeros(1,size(Xsel,1)),Xsel',Ytrain','linear');
                model{t}(s,k).const = coeff(1,2).const;
                model{t}(s,k).linear = coeff(1,2).linear;
            elseif strcmp(opt.classify.classifier,'qda')
                % Quadratic Discriminant Analysis
                [~,~,~,~,coeff] = classify(zeros(1,size(Xsel,1)),Xsel',Ytrain','quadratic');
                model{t}(s,k).const = coeff(1,2).const;
                model{t}(s,k).linear = coeff(1,2).linear;
                model{t}(s,k).quadratic = coeff(1,2).quadratic;
            end
            model{t}(s,k).ftr_type = opt.features.type;
            model{t}(s,k).ftrStats = ftrStats;
            fprintf('took %g seconds.\n',toc);
            
            %% Loop over test sets
            % Identify test sets
            testsets = setdiff(1:nD,trainsets(s,:));
            nTS = length(testsets);
            
            % Initialize results variables
            WinAcc = zeros(1,nTS);
            AUC = zeros(1,nTS);
            performance = cell(1,nTS);
            TSAcc = zeros(1,nTS);
            
            tic;
            fprintf('          Testing model using %g test set(s)... ',nTS);
            for test = 1:nTS
                % Get features and labels for current test set
                Xtest = cell2mat(FTRS(testsets(test)));
                Xseltest = Xtest(selected(1:K(k)),:);
                Ytest = double(cell2mat(LABS(testsets(test))));
                
                % Classify
                model{t}(s,k).Ytrue{test} = Ytest;
                Yhat = testCV(Xseltest,model{t}(s,k),opt);
                model{t}(s,k).Yhat{test} = Yhat;
                
                % Results
                WinAcc(test) = mean(Yhat==Ytest)*100;
                if (length(unique(Ytest))>1)
                    [~,~,~,AUC(test)] = perfcurve(Ytest',Yhat,1);
                else
                    AUC(test) = nan;
                end
                if (length(unique(Yhat))>1)&&(length(unique(Ytest))>1)
                    [~,~,~,per] = confusion(targtovec(Ytest)',targtovec(Yhat)');
                    performance{test} = per;
                else
                    performance{test} = nan*ones(4,1);
                end
                TSAcc(test) = accByTS(DATASETS{testsets(test)},Yhat,opt);
            end
            % Output Models
            Result{t}(s,k).WinAcc = WinAcc;
            Result{t}(s,k).TSacc = TSAcc;
            Result{t}(s,k).AUC = AUC;
            Result{t}(s,k).performance = performance;
            Result{t}(s,k).opt = opt;
            fprintf('took %g seconds.\n',toc);
            
            % Save Display result
            save(savename,'Result','-append')
            fprintf('          Result:\n'); 
            fprintf('               Accuracy by Windows: %g (%g) \n',mean(WinAcc),std(WinAcc));
            fprintf('               Accuracy by Time-Series: %g (%g) \n',mean(TSAcc),std(TSAcc));
            fprintf('               AUC by Windows: %g (%g) \n',mean(AUC),std(AUC));
        end
    end
end

