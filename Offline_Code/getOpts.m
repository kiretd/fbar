function opt = getOpts(opt)
% DATA OPTIONS
%  - opt.data.chan: integer or vector identifying EEG channels to use in
%    building and testing artifact models (default: 1, fbcsp will default
%    to all channels)
%  - opt.data.realtime: regular offline processing (0) or simulate
%    realtime (1)
%  - opt.data.testset: cell array of dataset names to use for
%    testing (default: {'Kiret1_IXDATA'})
%
% PLOT OPTIONS
%  (All default to 0, set to 1 to produce desired plots)
%  - opt.plot.raw: visually browse raw data
%  - opt.plot.timelock: plot averaged "ERP" (or time-locked signal for
%    non-ERP EEG data) in topological layout per class
%  - opt.plot.csp: plot the CSP component activity maps
%  - opt.plot.fft: plot the average FFT curve per class
%  - opt.plot.results: plot bar graphs of classification accuracy
%
% WINDOW OPTIONS
%  - opt.window.method: method for windowing the raw signals: (default: 'sliding')
%    'sliding': sliding time-window with winlen and step
%    'trials': epochs based on clean and artifactual segments
%  - opt.window.winlen: length of windows for feature extraction in seconds
%    (default: 1)
%  - opt.window.step: shift in windows for feature extraction in seconds
%    (default: 0.1)
%
% FILTERING OPTIONS
%  - opt.filter.bpfreq: frequency range for bandpass filter (default [2,40])
%
% FEATURE OPTIONS
%  - opt.features.K: vector of number of selected features to try
%    (e.g. 5:5:40)
%  - opt.features.type: type of feature extraction to use
%    ('realtimeEOG','sqitest','sqiAll','sqiEOG','sqiEMG','csp','fbcsp')
%  - opt.features.cspcomps: total number of components for csp, half of
%    total for fbcsp
%  - opt.features.cspband: global frequency range for fbcsp (e.g. [8,32])
%  - opt.features.cspstep: shift in frequency bands for fbcsp (e.g. 2)
%  - opt.features.cspwidth: width of frequency bands for fbcsp (e.g. 4)
%  - opt.features.fft: vector of fft bins for fft features (e.g. [8:2:30])
%  - opt.features.select: type of feature selection to use
%    ('all' (use all features),'mrmr_d','mrmr_q')
%
% CLASSIFICATION OPTIONS
%  - opt.classify.ntrain: number of training sets to use for cross-val
%    set to 'all' to get results for ntrain = 1 through (num Datasets - 1)
%  - opt.classify.class: scaler or vector of classes over which to classify
%    1=Clean, 2=LowFreq, 3=EMG_Hard, 4=EMG_Light, 5=ECR 6=Blinks
%  - opt.classify.classifier: type of classifier to use
%    'mlp' = Neural Network, 'svm' = Support Vector Machine,
%    'lda' = Linear Discriminant Analysis
%  - opt.classify.crossval: 1 (cross-validation among training sets), or
%    0 (use all training data to build model and test on test sets)
%
% NEURAL NETWORK OPTIONS
%
%
% SUPPORT VECTOR MACHINE OPTIONS

%%
optorig = opt;
% DATA OPTIONS
if ~isfield(opt,'data')
    opt.data = []; end
if ~isfield(opt.data,'chan')
    opt.data.chan = 1; end
if ~isfield(opt.data,'realtime')
    opt.data.realtime = 0; end
if ~isfield(opt.data,'testset')
    opt.data.testset = {'Kiret1_IXDATA'}; end

% PLOT OPTIONS
if ~isfield(opt,'plot')
    opt.plot = []; end
if ~isfield(opt.plot,'raw')
    opt.plot.raw = 0; end
if ~isfield(opt.plot,'windows')
    opt.plot.windows = 0; end
if ~isfield(opt.plot,'features')
    opt.plot.ftrs = 0; end
if ~isfield(opt.plot,'timelock')
    opt.plot.timelock = 0; end
if ~isfield(opt.plot,'csp')
    opt.plot.csp = 0; end
if ~isfield(opt.plot,'fft')
    opt.plot.fft = 0; end
if ~isfield(opt.plot,'testVapp')
    opt.plot.testVapp = 0; end
if ~isfield(opt.plot,'testVlabels')
    opt.plot.testVlabels = 0; end
if ~isfield(opt.plot,'comparison')
    opt.plot.comparison = 0; end
if ~isfield(opt.plot,'barfft')
    opt.plot.barfft = 0; end
if ~isfield(opt.plot,'testresult')
    opt.plot.testresult = 0; end

% WINDOW OPTIONS
if ~isfield(opt,'window')
    opt.window = []; end
if ~isfield(opt.window,'method')
    opt.window.method = 'sliding'; end
if ~isfield(opt.window,'winlen')
    opt.window.winlen = 1; end
if ~isfield(opt.window,'step')
    opt.window.step = 0.1; end

% FILTERING OPTIONS
if ~isfield(opt,'filter')
    opt.filter = []; end
if ~isfield(opt.filter,'bpfreq')
    opt.filter.bpfreq = [2 40]; end

% FEATURE OPTIONS
if ~isfield(opt,'features')
    opt.features = []; end
if ~isfield(opt.features,'type')
    opt.features.type = 'sqiEog'; end
if ~isfield(opt.features,'cspcomps')
    opt.features.cspcomps = 6; end
if ~isfield(opt.features,'cspband')
    opt.features.cspband = []; end
if ~isfield(opt.features,'cspstep')
    opt.features.cspstep = 2; end
if ~isfield(opt.features,'cspwidth')
    opt.features.cspwidth = 4; end
if ~isfield(opt.features,'K')
    opt.features.K = 5; end
if ~isfield(opt.features,'fft')
    opt.features.fft = []; end
if ~isfield(opt.features,'select')
    opt.features.select = 'mrmr_q'; end

% CLASSIFICATION OPTIONS
if ~isfield(opt,'classify')
    opt.classify = []; end
if ~isfield(opt.classify,'labels')
    opt.classify.labels = 'manual'; end
if ~isfield(opt.classify,'ntrain')
    opt.classify.ntrain = 1; end
if ~isfield(opt.classify,'class')
    opt.classify.class = 2; end
if ~isfield(opt.classify,'combinechans')
    opt.classify.combinechans = 0; end
if ~isfield(opt.classify,'classifier')
    opt.classify.classifier = 'svm'; end
if ~isfield(opt.classify,'crossval')
    opt.classify.crossval = 0; end
if ~isfield(opt.classify,'labelcutoff')
    opt.classify.labelcutoff = 0; end
if ~isfield(opt.classify,'removelabels')
    opt.classify.removelabels = 0; end

% NEURAL NETWORK OPTIONS
if strcmp(opt.classify.classifier,'mlp')
    if ~isfield(opt.classify,'mlp')
        opt.classify.mlp = []; end
    if ~isfield(opt.classify.mlp,'hidden')
        opt.classify.mlp.hidden = 50; end
    if ~isfield(opt.classify.mlp,'learningrate')
        opt.classify.mlp.learningrate = 0.01; end
end

% SVM OPTIONS
if strcmp(opt.classify.classifier,'svm')
    if ~isfield(opt.classify,'svm')
        opt.classify.svm = []; end
    if ~isfield(opt.classify.svm,'kernel')
        opt.classify.svm.kernel = 'rbf'; end
end

% Warn user if options were changed
if ~isequal(optorig,opt)
    warning('\nSome options were not set and have been set to default values.\n')
end