% example: artifacts5 is all bad, huge spikes in FFT at 40 Hz, 80 Hz, and 100 Hz

[b1,a1] = butter(6,[fq2dec(35,220),fq2dec(45,220)],'stop');
[b2,a2] = butter(6,[fq2dec(75,220),fq2dec(85,220)],'stop');
[b3,a3] = butter(6,[fq2dec(95,220),fq2dec(105,220)],'stop');

eegfilt = filter(b1,a1,eeg);
eegfilt = filter(b2,a2,eegfilt);
eegfilt = filter(b3,a3,eegfilt);