function R = compileResults(R,Test)

[nD,nK,nL] = size(R.err);

for d = 1:nD
    for l = 1:nL
        if ~isempty(Test{l})
            R.err(d,:,l) = Test{l}{d}.WinErr;
            R.auc(d,:,l) = Test{l}{d}.AUC;
            for k = 1:nK
                if length(unique(Test{l}{d}.Yhatcv{k})) > 1
                    [~,~,~,per] = confusion(targtovec(Test{l}{d}.Yhatcv{k})',...
                        targtovec(Test{l}{d}.testlabels{k})');
                    R.per(:,d,k,l) = per(1,:);
                end
            end
        end
    end
end