function err = correctedTimesError(Ytimes,Ytrue,class)

% Correct dimensionality of Ytimes
if size(Ytimes,1)>=size(Ytimes,2)
    Ytimes = Ytimes';
end

% % Mark data from other classes
% rmclass = setxor(class,[2,3,4]);
% rmtimes = zeros(1,length(Ytimes));
% for i = rmclass
%     rmtimes = rmtimes|Ytrue(i,:);
% end
% 
% % Remove other classes from data
% Ytimes(rmtimes) = [];
% Ytrue = Ytrue(class,:);
% Ytrue(rmtimes) = [];
Ytimes = Ytimes(Ytrue(1,:)==1|Ytrue(class,:)==1);
Ytrue = Ytrue(:,Ytrue(1,:)==1|Ytrue(class,:)==1);
Ytrue = Ytrue(class,:);

% Compute corrected Error
err = sum(-Ytimes==Ytrue)/length(Ytrue);