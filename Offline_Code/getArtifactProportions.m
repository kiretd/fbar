function [EOG,EMG] = getArtifactProportions(DATASETS)

nDS = length(DATASETS);
EOG = zeros(nDS,4);
EMG = zeros(nDS,4);
for ds = 1:nDS
    for chan = 1:4
        [~,Y] = feval([DATASETS{ds},'_ManualLabels'],chan);
        EOG(ds,chan) = mean(Y(2,:));
        EMG(ds,chan) = mean(Y(3,:)|Y(4,:));
    end
end
