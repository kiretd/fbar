function str = sortManualLabels(x)

x = sort(x);
X = zeros(max(x)+1,1);
X(x) = 1;
str = ytimes2string(X);