function [Result] = PlotModelResults2(model,opt)
class = opt.classify.class;
% if opt.classify.combinechans == 1
%     opt.data.chan = 1:4;
% elseif opt.classify.combinechans == 2
%     if opt.data.chan(1)==1 || opt.data.chan(1)==4
%         opt.data.chan = [1,4];
%     elseif opt.data.chan(1)==2 || opt.data.chan(1)==3
%         opt.data.chan = 2:3;
%     end
% end
opt = getOpts(opt);
chan = opt.data.chan;

% In case testdata wasn't defined as a cell array or contains empty entries
if ~iscell(opt.data.testset)
    opt.data.testset = {opt.data.testset};
else
    opt.data.testset = opt.data.testset(~cellfun('isempty',opt.data.testset));
end

% Loop over all test sets in cell array
for ts = 1:length(opt.data.testset)
    clear Xftrs Xtest IXDATA EEG Y winlen TStest2 TStest Yhat
    TrainLabelFlag = exist([opt.data.testset{ts},'_ManualLabels'],'file') == 2;
    
    %% Load dataset
    if TrainLabelFlag
        [IXDATA,Y] = feval([opt.data.testset{ts},'_ManualLabels'],chan);
        if exist([opt.data.testset{ts},'.mat'],'file')
            load(opt.data.testset{ts});
        end
    else
        load(opt.data.testset{ts})
        if ~exist('IXDATA','var')&&exist('EEG','var')
            eeg = concatenateEEG(EEG);
            IXDATA = convert2IXDATA(eeg);
            save(opt.data.testset{ts},'IXDATA','-append')
        elseif ~exist('IXDATA','var')&&exist('muse_eeg_raw','var')
            % Old version of Muse data
            IXDATA.raw.eeg.times = muse_eeg_raw(:,1); %#ok<NODEF>
            IXDATA.raw.eeg.data = muse_eeg_raw(:,2:5);
            save(opt.data.testset{ts},'IXDATA','-append')
        end
    end
    
    %% Prepare Data
    X = IXDATA.raw.eeg.data(1:end,chan);
    [TS,FS] = fixTS(IXDATA.raw.eeg.times(1:end));
    
    winlen = ceil(FS*opt.window.winlen);
    step = floor(FS*opt.window.step);
    nWins = length(1:step:(length(X)-winlen-step));
    
    if ~exist(['testftrs_',opt.features.type],'var')
        % Create feature variable and save for later
        eval(sprintf('testftrs_%s=cell(4,1);',opt.features.type));
        save(opt.data.testset{ts},['testftrs_',opt.features.type],'-append');
        eval(sprintf('testfft_%s=cell(4,1);',opt.features.type));
        save(opt.data.testset{ts},['testfft_',opt.features.type],'-append');
        eval(sprintf('testlabels_%s=cell(4,1);',opt.features.type));
        save(opt.data.testset{ts},['testlabels_',opt.features.type],'-append');
    end
    
    if isempty(eval(['testftrs_',opt.features.type,'{chan}']))
        tic;
        fprintf('Computing features... ');
        %% Extract features
        % Initialize features matrices if needed
        if strcmpi(opt.features.type,'sqi+')
            Xftrs = zeros(40,nWins);
        elseif strcmpi(opt.features.type,'sqiEogWithTheta')
            Xftrs = zeros(30,nWins);    
        elseif strcmpi(opt.features.type,'sqiEog')
            Xftrs = zeros(28,nWins);
        elseif strcmp(opt.features.type,'sqiEogNoRaw')
            Xftrs = zeros(24,nWins);
        elseif strcmpi(opt.features.type,'sqiEogNoAlpha')
            Xftrs = zeros(22,nWins);
        elseif strcmpi(opt.features.type,'sqiEmg')
            Xftrs = zeros(18,nWins);
        elseif strcmpi(opt.features.type,'sqiAll')
            Xftrs = zeros(54,nWins);
        end
        labels = zeros(1,nWins);
        if mod(FS/2,1) == 0
            Xfft = squeeze(zeros(floor(FS/2),length(chan),nWins));
        else
            Xfft = squeeze(zeros(floor(FS/2)+1,length(chan),nWins));
        end
        %     labwin = zeros(1,winlen);
        %     labwin((floor(winlen*0.1)+1):(winlen-floor(winlen*0.1))) = 1;
        
        % Extract features and window indices
        testTSplot = zeros(1,nWins);
        TStest2 = zeros(1,nWins);
        for i = 1:nWins
            ind = i*step;
            testTSplot(i) = floor(ind+winlen/2);
            TStest2(i) = TS(floor(ind+winlen/2))-TS(1);
            if strcmp(opt.features.type,'handcode')
                Xftrs(:,i) = buildftrs(X(ind:ind+winlen-1,:),newlab(i),chan,FS,0);
            elseif strcmp(opt.features.type,'sqi')
                Xftrs(:,i) = sqi_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqi+')
                Xftrs(:,i) = sqiplus_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'ar')
                Xftrs(:,i) = getARfeatures(X(ind:ind+winlen-1)',4,1);
            elseif strcmp(opt.features.type,'raw')
                Xftrs(:,i) = (X(ind:ind+winlen-1,chan)-mean(X(ind:ind+winlen-1,chan)))';
            elseif strcmp(opt.features.type,'sqi+-ar')
                Xftrs(:,i) = [sqiplus_ftrs(X(ind:ind+winlen-1),FS)',getARfeatures(X(ind:ind+winlen-1,chan)',4,1)];
            elseif strcmp(opt.features.type,'sqinew')
                Xftrs(:,i) = sqinew_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqiEog')
                Xftrs(:,i) = sqiEog_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqiEogNoRaw')
                Xftrs(:,i) = sqiEogNoRaw_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqiEogWithTheta')
                Xftrs(:,i) = sqiEogWithTheta_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqiEogNoAlpha')
                Xftrs(:,i) = sqiEogNoAlpha_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqiEmg')
                Xftrs(:,i) = sqiEmg_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqiAll')
                Xftrs(:,i) = sqiAll_ftrs(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'sqitest')
                Xftrs(:,i) = sqitest(X(ind:ind+winlen-1),FS)';
            elseif strcmp(opt.features.type,'fbcsp')
                Xftrs(:,:,i) = bsxfun(@minus,X(ind:ind+winlen-1,:),mean(X(ind:ind+winlen-1,:)))';
            end
            if TrainLabelFlag
                labels(i) = mean(Y(class,ind:ind+winlen-1)) > opt.classify.labelcutoff;
            else
                labels(i)= 0;
            end
            
            % Compute FFTs for FFT plots
            H = hamming(winlen);
            %         HP = sum(hamming(winlen)).^2;
            xtemp = bsxfun(@minus,X(ind:ind+winlen-1),mean(X(ind:ind+winlen-1)));
            psd = (fft(bsxfun(@times,H,xtemp)))/winlen;%(winlen*HP);
            if floor(FS/2) == size(Xfft,1)
                if ndims(Xfft) == 2 %#ok<*ISMAT>
                    Xfft(:,i) = abs(psd(1:floor(FS/2))).^2;
                elseif ndims(Xfft) == 3
                    Xfft(:,:,i) = abs(psd(1:floor(FS/2),:)).^2;
                end
            else
                if ndims(Xfft) == 2
                    Xfft(:,i) = abs(psd(1:floor(FS/2)+1,:)).^2;
                elseif ndims(Xfft) == 3
                    Xfft(:,:,i) = abs(psd(1:floor(FS/2)+1,:)).^2;
                end
            end
        end
        
        % Compute FBCSP features outside of loop
        if strcmp(opt.features.type,'fbcsp')
            opt.features.cspcomps = opt.features.cspcomps/2;
            opt.filter.type = 'butter';
            opt.filter.order = 4;
            opt.filter.sr = FS;
            opt.mode = 'ourcat';
            
            csp_trainind = logical(ones(nWins,1));
            if opt.data.numClasses == 2
                [nCSP,nChans,nBands] = size(opt.filter.cspfilters);
                Xcsp = zeros(nCSP*nBands,size(Xftrs,2),nWins);
                Xcspftrs = zeros(nCSP*nBands,nWins);
                for band = 1:size(opt.filter.cspfilters,3)
                    bandInds = (nCSP*(band-1)+1):band*nCSP;
                    CSPfilter = squeeze(opt.filter.cspfilters(:,:,band));
                    for i = 1:nWins
                        Xcsp(bandInds,:,i) = CSPfilter*Xftrs(:,:,i);
                        xvars = var(Xcsp(bandInds,:,i),[],2);
                        Xcspftrs(bandInds,i) = log10(xvars/sum(xvars));
                    end
                end
            elseif opt.data.numClasses >= 2
                % Placeholder for multiclass FBCSP
            end
            Xftrs = Xcspftrs;
        end
        
        % Normalize test ftrs
        if isfield(model,'ftrStats')
            if isfield(model.ftrStats,'std')
                ftrs_norm = zeros(size(Xftrs));
                for f = 1:size(Xftrs,1)
                    if iscell(model.ftrStats.mean)
                        %             test_input(:,f) = zscore(testfeats(:,f));
                        ftrs_norm(f,:) = bsxfun(@minus,Xftrs(f,:),model.ftrStats.mean{class}(f));
                        ftrs_norm(f,:) = bsxfun(@rdivide,ftrs_norm(f,:),model.ftrStats.std{class}(f));
                    else
                        ftrs_norm(f,:) = bsxfun(@minus,Xftrs(f,:),model.ftrStats.mean(f));
                        ftrs_norm(f,:) = bsxfun(@rdivide,ftrs_norm(f,:),model.ftrStats.std(f));
                    end
                end
            end
        end
        % Save the feature set for later use
        eval(sprintf('testftrs_%s{chan}=ftrs_norm;',opt.features.type));
        save(opt.data.testset{ts},['testftrs_',opt.features.type],'-append');
        eval(sprintf('testfft_%s{chan}=Xfft;',opt.features.type));
        save(opt.data.testset{ts},['testfft_',opt.features.type],'-append');
        eval(sprintf('testlabels_%s{chan}=labels;',opt.features.type));
        save(opt.data.testset{ts},['testlabels_',opt.features.type],'-append');
        save(opt.data.testset{ts},'testTSplot','-append');
        fprintf('Took %gs\n',toc);
    end
    
    %% Run Model
    tic;
    fprintf('Running model... ');
    % Extract relevant features, ffts, and labels
    ftrs_norm = eval(sprintf('testftrs_%s{chan}',opt.features.type));
    labels = eval(sprintf('testlabels_%s{chan}',opt.features.type));
    Xfft = eval(sprintf('testfft_%s{chan}',opt.features.type));
    
    % Feature Selection
    if isfield(model.ftrStats,'selected')
        ftrs_norm = ftrs_norm(model.ftrStats.selected,:);
    end
    
    % Classify Test Data
    if strcmp(opt.classify.classifier,'mlp')
        [er,bad,Yhat] = nntest(model,ftrs_norm', targtovec(double(labels)));
    elseif strcmp(opt.classify.classifier,'svm')
%         Yhat = svmclassify(model,ftrs_norm');
%         Yhat(isnan(Yhat)) = 1;
%         Yhat = -(Yhat-1);
        nData = size(ftrs_norm,2);
        nDivs = floor(nData/1000);
        remainder = rem(nData,1000);
        Yhat = zeros(1,nData);
        for i = 1:nDivs
            inds = (1000*(i-1)+1):1000*i;
            Yhat(inds) = svmclassify(model,ftrs_norm(:,inds)');
        end
        if remainder > 0
            inds = (1000*nDivs+1):nData;
            Yhat(inds) = svmclassify(model,ftrs_norm(:,inds)');
        end
        Yhat(isnan(Yhat)) = 1;
        if mean(Yhat~=labels)>mean(Yhat==labels)
            Yhat = -(Yhat-1);
        end
    elseif strcmp(opt.classify.classifier,'lda')
        Yhat = (model.const + ftrs_norm'*model.linear) > 0;
    elseif strcmp(opt.classify.classifier,'qda')
        %         Yhat = (model.const + testftrs_norm'*model.linear + ...
        %             testftrs_norm'*model.quadratic*testftrs_norm) > 0;
        Yhat = zeros(nWins,1);
        linterm = model.const + ftrs_norm'*model.linear;
        for t = 1:nWins
            Yhat(t) = (linterm(t) + ...
                ftrs_norm(:,t)'*model.quadratic*ftrs_norm(:,t)) > 0;
        end
    end
    
    % Convert classification results to time-series
    Ytimes = zeros(length(X),1);
    Ytimes(1:testTSplot(1)) = Yhat(1);
    for ep = 2:nWins-1
        Ytimes(testTSplot(ep-1):testTSplot(ep)) = Yhat(ep);
        if (Yhat(ep)~=Yhat(ep-1))&&(Yhat(ep)~=Yhat(ep+1))
            Ytimes(testTSplot(ep-1):testTSplot(ep)) = Yhat(ep-1);
        end
    end
    Ytimes(testTSplot(ep):end) = Yhat(nWins);
    retained = mean(Ytimes==0);
    
    %     % Convert test labels to time-series labels if available
    %     if opt.classify.crossval
    %         tslabels = zeros(length(Xtest),1);
    %         tslabels(1:TStest(1)) = model.testlabels(1)-1;
    %         for ep = 2:nWins-1
    %             tslabels(TStest(ep-1):TStest(ep)) = model.testlabels(ep)-1;
    %             if (model.testlabels(ep)~=model.testlabels(ep-1))&&(model.testlabels(ep)~=model.testlabels(ep+1))
    %                 tslabels(TStest(ep-1):TStest(ep)) = Yhat(ep-1)-1;
    %             end
    %         end
    %     end
    
    Result.Yhat{ts,chan} = Yhat;
    Result.Ytimes{ts,chan} = Ytimes';
    if exist('Y','var')
        Result.Ytrue{ts,chan} = Y;
    end
    Result.model_retained(ts,chan) = retained;
    fprintf('Took %gs\n',toc);
    
    %% PLOTS
    for ch = chan
        if opt.plot.testVapp
            %% Output of current app version versus Current Model
            h = figure;
            
            do_microVolts_conv = (IXDATA.raw.eeg.data(:) == floor(IXDATA.raw.eeg.data(:)));
            if all(do_microVolts_conv) % convert to micro-volts
                SNR_threshold = 150;
            else
                SNR_threshold = 150*(1.644980342484907^2);
                if any(do_microVolts_conv)
                    % 	        disp(['ERROR #9 : Part of EEG data is not in micro Volts for ' thisSessionFilePath]);
                end
            end
            filtInfo = filter_2_36_bp_order6();
            IXDATA = computeSigPower(IXDATA,'thres',SNR_threshold,'filter',[filtInfo.A; filtInfo.B],'window',256);
            badIX = IXDATA.raw.eeg.badsections{chan};
            %     retainedAPP = 0;
            APPtimes = zeros(size(Ytimes));
            for bb = 1:size(badIX,1)
                [xq,badtimesIX(bb,1)] = min(abs(badIX(bb,1)-TS));
                [xq,badtimesIX(bb,2)] = min(abs(badIX(bb,2)-TS));
                APPtimes(badtimesIX(bb,1):badtimesIX(bb,2)) = 1;
                %         retainedAPP = retainedAPP + length(badtimesIX(bb,1):badtimesIX(bb,2));
            end
            %     retainedAPP = 1 - (retainedAPP / length(TS));
            retainedAPP = 1 - mean(APPtimes);
            MLP_APP_agreement = mean(APPtimes==Ytimes);
            
            % Plot Classifier output for MLP and APP version
            sh(1) = subplot(5,1,1);
            goodraw = X; goodraw(Ytimes==1) = nan;
            badraw = X; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('MLP classification output')
            
            sh(2) = subplot(5,1,2);
            goodlabels = X; goodlabels(APPtimes==1) = nan;
            badlabels = X; badlabels(APPtimes==0) = nan;
            plot(TS-TS(1),goodlabels,'b');
            hold on;
            plot(TS-TS(1),badlabels,'r')
            title('APP VERSION classification output')
            
            % Plot predicted labels
            sh(3) = subplot(5,1,3);
            plot(TStest2,Yhat)
            ylim([0,3])
            linkaxes(sh,'x')
            
            % Plot FFTs
            if length(size(Xfft)) == 2
                Xfft_clean = 10*log10(mean(Xfft(:,Yhat==1),2));
                Xfft_bad = 10*log10(mean(Xfft(:,Yhat==2),2));
                Xfft_mean = 10*log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_clean = 10*log10(mean(Xfft(:,:,Yhat==1),3));
                Xfft_bad = 10*log10(mean(Xfft(:,:,Yhat==2),3));
                Xfft_mean = 10*log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            subplot(5,1,4:5);
            plot(Fr,Xfft_mean(:,1:length(chan)),'b')
            hold on
            plot(Fr,Xfft_clean(:,1:length(chan)),'g')
            hold on
            plot(Fr,Xfft_bad(:,1:length(chan)),'r')
            hold on
            viewFft(IXDATA,'chans',ch,'hold',1,'color','k','clean',1);
            legend('Total','Clean','Bad','APP')
            title('FFTs')
            
            fprintf('MLP_retained: %g APP_retained: %g Agreement: %g \n',...
                retained,retainedAPP,MLP_APP_agreement);
            
            Result.APP_retained(ts,chan) = retainedAPP;
            Result.MLP_APP_agreement(ts,chan) = MLP_APP_agreement;
        end
        
        if opt.plot.testVlabels && TrainLabelFlag
            %% Output of Current Model versus Labeled Data
            h2 = figure;
            
            % Identify samples which do not belong to relevant classes
            if class ~= 1
                rmclass = setxor(class,[2,3,4]);
                rmtimes = zeros(length(Ytimes),1);
                for rm = rmclass
                    rmtimes = rmtimes|Y(rm,:)';
                end
                rmtimes = Y(1,:)|Y(class,:);
            else
                rmtimes = ones(1,length(Y));
%                 Y(1,:) = double(-(Y(1,:)-1));
            end
            
            % Plot Classifier output for MLP and APP version
            sh2(1) = subplot(4,1,1);
            goodraw = X; goodraw(Ytimes==1) = nan;
            badraw = X; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('Model Classification Output')
            legend('Clean','Artifact')
            
%             sh2(2) = subplot(4,1,2);
%             goodlabels = X; goodlabels(Y(class,:)==1) = nan;
%             badlabels = X; badlabels(Y(class,:)==0) = nan;
%             rmlabels = X; rmlabels(rmtimes,:) = nan;
%             plot(TS-TS(1),goodlabels,'b');
%             hold on;
%             plot(TS-TS(1),badlabels,'r')
%             plot(TS-TS(1),rmlabels,'c')
%             title('True Labels')

            sh2(2) = subplot(4,1,2);
            cleanlabels = X;
            EOGlabels = X; EOGlabels(Y(2,:)==0) = nan;
            EMGlabels = X; EMGlabels((Y(3,:)==0)&(Y(4,:)==0)) = nan;
            plot(TS-TS(1),cleanlabels,'b');
            hold on;
            plot(TS-TS(1),EOGlabels,'r')
            plot(TS-TS(1),EMGlabels,'k')
            title('True Labels')
            
            % Plot FFTs
            if length(size(Xfft)) == 2
                Xfft_model = 10*log10(mean(Xfft(:,Yhat==0),2));
                Xfft_true = 10*log10(mean(Xfft(:,labels==0),2));
                Xfft_mean = 10*log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_model = 10*log10(mean(Xfft(:,:,Yhat==0),3));
                Xfft_true = 10*log10(mean(Xfft(:,:,labels==0),3));
                Xfft_mean = 10*log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            sh2(3) = subplot(4,1,3:4);
            plot(Fr,Xfft_mean(:,1:length(chan)),'k','LineWidth',2)
            hold on
            plot(Fr,Xfft_model(:,1:length(chan)),'b','LineWidth',2)
            hold on
            plot(Fr,Xfft_true(:,1:length(chan)),'g','LineWidth',2)
            legend('Total','Model','Labels')
            title('FFTs')
            linkaxes(sh2(1:2),'xy')
            
            Result.fft_corr = corr(Xfft_model,Xfft_true);
            fprintf('FFT Correlation: %g\n',Result.fft_corr);
            
            if opt.plot.barfft
                figure;
                bar(Fr,Xfft_mean,1,'b','BaseValue',-4); hold on
                bar(Fr,Xfft_model,1,'g','BaseValue',-4);
            end
            
        end
        
        if opt.plot.adjustlabels && TrainLabelFlag
            %% Output of Current Model versus Labeled Data
            h3 = figure;
            
            % Identify samples which do not belong to relevant classes
            rmclass = setxor(class,[2,3,4]);
            rmtimes = zeros(length(Ytimes),1);
            for rm = rmclass
                rmtimes = rmtimes|Y(rm,:)';
            end
            rmtimes = Y(1,:)|Y(class,:);
            
            % Plot Classifier output for MLP and APP version
            sh3(1) = subplot(4,1,1);
            goodraw = X; goodraw(Ytimes==1) = nan;
            badraw = X; badraw(Ytimes==0) = nan;
            plot(goodraw,'b');
            hold on;
            plot(badraw,'r')
            title('Model Classification Output')
            
            sh3(2) = subplot(4,1,2);
            goodlabels = X; goodlabels(Y(class,:)==1) = nan;
            badlabels = X; badlabels(Y(class,:)==0) = nan;
            rmlabels = X; rmlabels(rmtimes,:) = nan;
            plot(goodlabels,'b');
            hold on;
            plot(badlabels,'r')
            plot(rmlabels,'c')
            title('True Labels')
            
            % Plot Ytimes
            sh3(3) = subplot(4,1,3);
            Yplot = zeros(1,length(Y));
            for pp = 2:4; Yplot(Y(pp,:)==1) = pp; end;
            plot(Yplot)
            ylim([-1,5])
            linkaxes(sh3(1:2),'xy')
            linkaxes(sh3(1:3),'x')
        end
        
        if opt.plot.fft && TrainLabelFlag
            %% Plot FFTs
            if length(size(Xfft)) == 2
                Xfft_model = 10*log10(mean(Xfft(:,Yhat==0),2));
                Xfft_true = 10*log10(mean(Xfft(:,labels==0),2));
                Xfft_mean = 10*log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_model = 10*log10(mean(Xfft(:,:,Yhat==0),3));
                Xfft_true = 10*log10(mean(Xfft(:,:,labels==0),3));
                Xfft_mean = 10*log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            hfft = figure;
            plot(Fr,Xfft_mean(:,1:length(chan)),'k','LineWidth',3)
            hold on
            plot(Fr,Xfft_model(:,1:length(chan)),'b','LineWidth',3)
            hold on
            plot(Fr,Xfft_true(:,1:length(chan)),'g','LineWidth',3)
            legend('Original','FBAR','Training Labels')
            title('FFT Comparison')
        end
        
        if opt.plot.fft && opt.plot.comparison %~TrainLabelFlag
            %% Plot FFTs
            if length(size(Xfft)) == 2
                Xfft_model = 10*log10(mean(Xfft(:,Yhat==0),2));
                Xfft_mean = 10*log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_model = 10*log10(mean(Xfft(:,:,Yhat==0),3));
                Xfft_mean = 10*log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            % Get faster FFTs
            [yfaster,~] = useFaster('faster+',opt.data.testset(ts));
            yfaster = yfaster{1}(ch,:);
            yfasterwin = zeros(1,nWins);
            H = hamming(winlen);
            for w = 1:nWins
                ind = w*step;
                yfasterwin(w) = mean(yfaster(ind:ind+winlen-1)) > opt.classify.labelcutoff;
            end
            Xfft_faster = 10*log10(mean(Xfft(:,yfasterwin==0),2));
            
            % Plot
            hfft = figure;
            plot(Fr,Xfft_mean(:,1:length(chan)),'k','LineWidth',3)
            hold on
            plot(Fr,Xfft_model(:,1:length(chan)),'b','LineWidth',3)
            plot(Fr,Xfft_faster(:,1:length(chan)),'m','LineWidth',3)
            legend('Original','FBAR','FASTER+')
            title('PSD Comparison')
        end
        
        if opt.plot.comparison && TrainLabelFlag
            %% Output of Current Model versus FASTER+ and Labeled Data
            hcompare = figure;

            % Plot Classifier outputs
            shcompare(1) = subplot(4,1,1);
            goodraw = X; goodraw(Ytimes==1) = nan;
            badraw = X; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('FBAR Output')
            legend('Clean','Artifacts')
            
            shcompare(2) = subplot(4,1,2);
            [yfaster,~] = useFaster('faster+',opt.data.testset(ts));
            yfaster = yfaster{1}(ch,:);
            goodlabels = X; goodlabels(yfaster==1) = nan;
            badlabels = X; badlabels(yfaster==0) = nan;
            plot(TS-TS(1),goodlabels,'b');
            hold on;
            plot(TS-TS(1),badlabels,'r')
            title('FASTER+ Output')

%             shcompare(3) = subplot(3,1,3);
%             cleanlabels = X;
%             EOGlabels = X; EOGlabels(Y(2,:)==0) = nan;
%             EMGlabels = X; EMGlabels((Y(3,:)==0)&(Y(4,:)==0)) = nan;
%             plot(TS-TS(1),cleanlabels,'b');
%             hold on;
%             plot(TS-TS(1),EOGlabels,'r')
%             plot(TS-TS(1),EMGlabels,'k')
%             title('Training Labels')
            
            % Plot FFTs
            yfasterwin = zeros(1,nWins);
            H = hamming(winlen);
            for w = 1:nWins
                ind = w*step;
                yfasterwin(w) = mean(yfaster(ind:ind+winlen-1)) > opt.classify.labelcutoff;
            end
            Xfft_faster = 10*log10(mean(Xfft(:,yfasterwin==0),2));
            
            if length(size(Xfft)) == 2
                Xfft_model = 10*log10(mean(Xfft(:,Yhat==0),2));
                Xfft_true = 10*log10(mean(Xfft(:,labels==0),2));
                Xfft_mean = 10*log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_model = 10*log10(mean(Xfft(:,:,Yhat==0),3));
                Xfft_true = 10*log10(mean(Xfft(:,:,labels==0),3));
                Xfft_mean = 10*log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            shcompare(3) = subplot(4,1,3:4);
            plot(Fr,Xfft_mean(:,1:length(chan)),'k','LineWidth',3)
            hold on
            plot(Fr,Xfft_model(:,1:length(chan)),'b','LineWidth',3)
            hold on
            plot(Fr,Xfft_faster(:,1:length(chan)),'m','LineWidth',3)
            plot(Fr,Xfft_true(:,1:length(chan)),'g','LineWidth',3)
            legend('Original','FBAR','FASTER+','Labels')
            title('PSDs')
            
            linkaxes(shcompare(1:2),'xy')
        end
        
        if opt.plot.comparison && ~TrainLabelFlag
            %% Output of Current Model versus FASTER+ and Labeled Data
            hcompare = figure;

            % Plot Classifier outputs
            shcompare(1) = subplot(4,1,1);
            goodraw = X; goodraw(Ytimes==1) = nan;
            badraw = X; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('FBAR Output')
            legend('Clean','Artifact')
            
            shcompare(2) = subplot(4,1,2);
            [yfaster,~] = useFaster('faster+',opt.data.testset(ts));
            yfaster = yfaster{1}(ch,:);
            goodlabels = X; goodlabels(yfaster==1) = nan;
            badlabels = X; badlabels(yfaster==0) = nan;
            plot(TS-TS(1),goodlabels,'b');
            hold on;
            plot(TS-TS(1),badlabels,'r')
            title('FASTER+ Output')
            
            % Plot FFTs
            yfasterwin = zeros(1,nWins);
            H = hamming(winlen);
            for w = 1:nWins
                ind = w*step;
                yfasterwin(w) = mean(yfaster(ind:ind+winlen-1)) > opt.classify.labelcutoff;
            end
            Xfft_faster = 10*log10(mean(Xfft(:,yfasterwin==0),2));
            
            if length(size(Xfft)) == 2
                Xfft_model = 10*log10(mean(Xfft(:,Yhat==0),2));
                Xfft_mean = 10*log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_model = 10*log10(mean(Xfft(:,:,Yhat==0),3));
                Xfft_mean = 10*log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            shcompare(3) = subplot(4,1,3:4);
            plot(Fr,Xfft_mean(:,1:length(chan)),'k','LineWidth',3)
            hold on
            plot(Fr,Xfft_model(:,1:length(chan)),'b','LineWidth',3)
            hold on
            plot(Fr,Xfft_faster(:,1:length(chan)),'m','LineWidth',3)
            legend('Total','Model','Labels')
            title('PSDs')

            legend('Original','FBAR','FASTER+')
            linkaxes(shcompare(1:2),'xy')
        end
        
        if opt.plot.testresult
            %% Output just the result from this channel
            htest = figure;
            
            % Plot Classifier outputs
            shcompare(1) = subplot(4,1,1);
            goodraw = X; goodraw(Ytimes==1) = nan;
            badraw = X; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('Model Output')
            legend('Clean','Artifact')
        end
        
        if opt.plot.TestAllModels
            %% Output of Model built with each Classifier
            
        end
        
    end
end