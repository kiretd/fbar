load Results_QDA_ch4_allK_allL_cv_1

%% Basic stats
[nSets,nFtrs,nL] = size(R.err);
Acc = (1-R.err)*100;
meanAcc = squeeze(mean(Acc));
stdAcc = squeeze(std(Acc));
steAcc = stdAcc/sqrt(nSets);

%% Accuracy by Cutoff
[maxmeanAcc,inds] = max(meanAcc);
for i = 1:nL; ste1(i) = steAcc(inds(i),i); end
figure;
props = {'b','LineWidth',2};
h1 = shadedErrorBar((0:nL-1)*0.1,maxmeanAcc,ste1,props,1);
ylabel('% Accuracy','FontSize',16)
xlabel('Cutoff Parameter','FontSize',16)
axis([0,(nL-1)*0.1,80,100])

%% Accuracy by Num Features
[~,maxInd] = max(maxmeanAcc);
maxFtrs = inds(maxInd);
figure;
props = {'b','LineWidth',2};
h1 = shadedErrorBar(1:nFtrs,meanAcc(:,maxInd),steAcc(:,maxInd),props,1); 
ylabel('% Accuracy','FontSize',16)
xlabel('# of Selected Features','FontSize',16)
axis([1,nFtrs,90,100]);

%% Accuracy without other classes
class = 2;
rmclass = [3,4];
chan = 3;

[nD,nK,nL] = size(R.err);
Rcorrected.err = zeros(nD,nK,nL);
Rcorrected.auc = zeros(nD,nK,nL);
Rcorrected.per = zeros(4,nD,nK,nL);
[~,Y{1}] = artifacts_david_orig_ManualLabels(chan); 
[~,Y{2}] = artifacts_saurabh_ManualLabels(chan); 
[~,Y{3}] = artifacts_kiret_ManualLabels(chan); 

for d = 1:nD
    for l = 1:nL
        if ~isempty(Test{l})
            for k = 1:nK
                predlabels = -Test{l}{d}.Ytimes{k}{4};
                truelabels = Y{d}(class,:)';
                keeplabels = ~(Y{d}(rmclass(1),:)|Y{d}(rmclass(2),:));
                predlabels = predlabels(keeplabels);
                truelabels = truelabels(keeplabels);
                Rcorrected.err(d,:,l) = sum(predlabels==truelabels)/sum(keeplabels);
                %             Rcorrected.auc(d,:,l) = Test{l}{d}.AUC;
                if length(unique(Test{l}{d}.Yhatcv{k})) > 1
                    [~,~,~,per] = confusion(targtovec(predlabels)',targtovec(truelabels)');
                    R.per(:,d,k,l) = per(1,:);
                end
            end
        end
    end
end
