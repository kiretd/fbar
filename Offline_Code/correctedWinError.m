function err = correctedWinError(Yhat,Ytrue,class,cutoff)

% Correct dimensionality of Ytimes
% if size(Ytimes,1)>=size(Ytimes,2)
%     Ytimes = Ytimes';
% end

% Convert other class labels to windows
rmclass = setxor(class,[2,3,4]);


% Mark data from other classes
rmwins = zeros(1,length(Yhat));
for i = rmclass
    rmwins = rmwins|Ytrue(i,:);
end

% Remove other classes from data
Ytimes(rmwins) = [];
Ytrue = Ytrue(class,:);
Ytrue(rmwins) = [];

% Compute corrected Error
err = sum(-Ytimes==Ytrue)/length(Ytrue);