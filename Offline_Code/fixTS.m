function [TS_fixed, Fs] = fixTS(ts, exp_Fs)
% [TS_fixed, Fs] = fixTS(ts, exp_Fs)
%
% Fixes OSC timestamps, outputs evenly sampled times and sample rate.
% Raises warning if large gaps are detected in the data.
%
% INPUTS
%  ts       : input timestamp array
%  exp_Fs   : (optional) expected Fs
%
% OUTPUTS
%  TS_fixed : fixed timepstamp array (uniformly sampled)
%  Fs       : new empirical sampling frequency
%

MAX_GAP = 1; % seconds
MAX_dT = 0.3; % std/mean ratio

T = (ts(end)-ts(1))/(length(ts)-1);
sdT = std(filter(ones(1,100)/100, 1, diff(ts)));
dT = diff(ts);

times = {ts};
%check for large gaps
if ~isempty(find(dT>=MAX_GAP,1))

    %large gap somewhere, fix in sections         
    gap_inds = find(dT>=MAX_GAP);
    
%     if ~isdeployed
%         fprintf('Warning: Large gap detected at \n');
%         fprintf('  %f\n', ts(gap_inds));
%     end

    cur_ind = 1;
    times = cell(length(gap_inds)+1,1);
    
    for i = 1:length(gap_inds)+1
        if i<=length(gap_inds)
            times{i} = ts(cur_ind:gap_inds(i));
            cur_ind = gap_inds(i)+1;
        else
            times{i} = ts(cur_ind:end);
        end
    end
end

for j = 1:size(times,1)
    try
        [new_ts{j}, new_Fs{j}] = createTimesDistribution(times{j}); 
        if exist('exp_Fs', 'var') && ~isempty(exp_Fs) && ~isdeployed
            fprintf('Expected sampling rate: %i. \n', exp_Fs);
            if abs(new_Fs{j} - exp_Fs)*1/new_Fs{j} >= MAX_dT
                fprintf('Warning: Expected and actual sampling rate differ significantly. \n')
                fprintf('Actual sampling rate: %i. \n', new_Fs);
            end    
        end
    catch
        new_ts{j} = times{j};
        new_Fs{j} = {0};
        continue
    end
end

%Reassemble Time Stamps into a single list
TS_fixed = [];
for k = 1:size(new_ts,2)
    TS_fixed = [TS_fixed new_ts{k}'];
end

TS_fixed = TS_fixed';
[m,index] = max(cellfun(@length,new_ts));
Fs = new_Fs{index};

% Check if gaps have been removed by fixing TS and rerun to smooth unjustified gaps
if exist('gap_inds','var')
    if length(find(diff(TS_fixed)>=MAX_GAP)) < length(gap_inds)
        [TS_fixed, Fs] = fixTS(TS_fixed);
    end
end

end


function [times, samplingRate] = createTimesDistribution(ts)
%
% Redistribute timestamps using the Fs estimated on the last 75% of the
% timestamps.
% Redistribute starting from the end of the timestamps.
    
% Estimation time, taking 75% of the total time to avoid initial clumps
length75th = floor(length(ts)*0.75);
ts75th = ts(end-length75th:end);
T = (ts75th(end)-ts75th(1))/length75th;
samplingRate = 1/T;

times = zeros(length(ts),1);
times(end) = ts(end);
for i = 1:length(ts)-1
    times(end-i) = ts(end)-i/samplingRate;
end

end