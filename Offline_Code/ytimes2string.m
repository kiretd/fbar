function string = ytimes2string(y)

% Identify where and how many times label changes
d = [y(1);diff(y)];
inds = find(d~=0);
N = length(inds);

% Write a string to identify the boundary indices
string = [];
for i = 1:N-1
    if (y(inds(i))==-1)||(y(inds(i))==1)
        string = [string,num2str(inds(i)),':',num2str(inds(i+1)-1),','];
    end
end
if (inds(end)==-1)||(inds(end)==1)
    string = [string,num2str(inds(end)),':',num2str(length(y))];
end