function [Result] = TestModel(model,opt)

% In case testdata wasn't defined as a cell array
if ~iscell(opt.data.testset)
    opt.data.testset = {opt.data.testset};
else
    opt.data.testset = opt.data.testset(~cellfun('isempty',opt.data.testset));
end

% Loop over all test sets in cell array
for ts = 1:length(opt.data.testset)
    clear Xftrs Xtest IXDATA Y winlen TStest2 TStest Yhat
    
    if opt.classify.crossval
        [IXDATA,Y] = feval([opt.data.testset{ts},'_ManualLabels'],opt.data.chan);
    else
        load(opt.data.testset{ts})
    end
    
    %% Prepare Test Data
    chan = opt.data.chan;
    class = opt.classify.class;
    
    if exist('muse_eeg_raw','var')
        % Correction for old version of Muse data
        IXDATA.raw.eeg.times = muse_eeg_raw(:,1); %#ok<NODEF>
        IXDATA.raw.eeg.data = muse_eeg_raw(:,2:5);
    end
    [TS,FS] = fixTS(IXDATA.raw.eeg.times(1:end));
    
    winlen = ceil(FS*opt.window.winlen);
    step = floor(FS*opt.window.step);
    
    Xtest = IXDATA.raw.eeg.data(1:end,chan);
    nWins = length(1:step:(length(Xtest)-winlen-step));
    
    %% Extract features
    % Initialize features matrices if needed
    if strcmp(opt.features.type,'sqi+')
        Xftrs = zeros(40,nWins);
    elseif strcmp(opt.features.type,'sqiEogNoRaw')
        Xftrs = zeros(24,nWins);
    elseif strcmp(opt.features.type,'sqiEog')
        Xftrs = zeros(28,nWins);
    elseif strcmp(opt.features.type,'sqiEmg')
        Xftrs = zeros(22,nWins);
    elseif strcmp(opt.features.type,'sqiAll')
        Xftrs = zeros(54,nWins);
    end
    testlabels = zeros(1,nWins);
    if mod(FS/2,1) == 0
        Xfft = squeeze(zeros(floor(FS/2),length(chan),nWins));
    else
        Xfft = squeeze(zeros(floor(FS/2)+1,length(chan),nWins));
    end
    %     labwin = zeros(1,winlen);
    %     labwin((floor(winlen*0.1)+1):(winlen-floor(winlen*0.1))) = 1;
    
    % Extract features and window indices
    TStest = zeros(1,nWins);
    TStest2 = zeros(1,nWins);
    for i = 1:nWins
        ind = i*step;
        TStest(i) = floor(ind+winlen/2);
        TStest2(i) = TS(floor(ind+winlen/2))-TS(1);
        if strcmp(opt.features.type,'handcode')
            Xftrs(:,i) = buildftrs(Xtest(ind:ind+winlen-1,:),newlab(i),chan,FS,0);
        elseif strcmp(opt.features.type,'sqi')
            Xftrs(:,i) = sqi_ftrs(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'sqi+')
            Xftrs(:,i) = sqiplus_ftrs(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'ar')
            Xftrs(:,i) = getARfeatures(Xtest(ind:ind+winlen-1)',4,1);
        elseif strcmp(opt.features.type,'raw')
            Xftrs(:,i) = (Xtest(ind:ind+winlen-1,chan)-mean(Xtest(ind:ind+winlen-1,chan)))';
        elseif strcmp(opt.features.type,'sqi+-ar')
            Xftrs(:,i) = [sqiplus_ftrs(Xtest(ind:ind+winlen-1),FS)',getARfeatures(Xtest(ind:ind+winlen-1,chan)',4,1)];
        elseif strcmp(opt.features.type,'sqinew')
            Xftrs(:,i) = sqinew_ftrs(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'sqiEog')
            Xftrs(:,i) = sqiEog_ftrs(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'sqiEmg')
            Xftrs(:,i) = sqiEmg_ftrs(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'sqiAll')
            Xftrs(:,i) = sqiAll_ftrs(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'sqitest')
            Xftrs(:,i) = sqitest(Xtest(ind:ind+winlen-1),FS)';
        elseif strcmp(opt.features.type,'fbcsp')
            Xftrs(:,:,i) = bsxfun(@minus,Xtest(ind:ind+winlen-1,:),mean(Xtest(ind:ind+winlen-1,:)))';
        end
        if opt.classify.crossval
            testlabels(i) = mean(Y(class,ind:ind+winlen-1)) > opt.classify.labelcutoff;
        else
            testlabels(i)= 0;
        end
        
        % Compute FFTs for FFT plots
        H = hamming(winlen);
        %         HP = sum(hamming(winlen)).^2;
        xtemp = bsxfun(@minus,Xtest(ind:ind+winlen-1),mean(Xtest(ind:ind+winlen-1)));
        psd = (fft(bsxfun(@times,H,xtemp)))/winlen;%(winlen*HP);
        if floor(FS/2) == size(Xfft,1)
            if ndims(Xfft) == 2 %#ok<*ISMAT>
                Xfft(:,i) = abs(psd(1:floor(FS/2))).^2;
            elseif ndims(Xfft) == 3
                Xfft(:,:,i) = abs(psd(1:floor(FS/2),:)).^2;
            end
        else
            if ndims(Xfft) == 2
                Xfft(:,i) = abs(psd(1:floor(FS/2)+1,:)).^2;
            elseif ndims(Xfft) == 3
                Xfft(:,:,i) = abs(psd(1:floor(FS/2)+1,:)).^2;
            end
        end
    end
    
    % Compute FBCSP features outside of loop
    if strcmp(opt.features.type,'fbcsp')
        opt.features.cspcomps = opt.features.cspcomps/2;
        opt.filter.type = 'butter';
        opt.filter.order = 4;
        opt.filter.sr = FS;
        opt.mode = 'ourcat';
        
        csp_trainind = logical(ones(nWins,1));
        if opt.data.numClasses == 2
            [nCSP,nChans,nBands] = size(opt.filter.cspfilters);
            Xcsp = zeros(nCSP*nBands,size(Xftrs,2),nWins);
            Xcspftrs = zeros(nCSP*nBands,nWins);
            for band = 1:size(opt.filter.cspfilters,3)
                bandInds = (nCSP*(band-1)+1):band*nCSP;
                CSPfilter = squeeze(opt.filter.cspfilters(:,:,band));
                for i = 1:nWins
                    Xcsp(bandInds,:,i) = CSPfilter*Xftrs(:,:,i);
                    xvars = var(Xcsp(bandInds,:,i),[],2);
                    Xcspftrs(bandInds,i) = log10(xvars/sum(xvars));
                end
            end
        elseif opt.data.numClasses >= 2
            % Placeholder for multiclass FBCSP
        end
        Xftrs = Xcspftrs;
    end
    
    % Normalize test ftrs
    if isfield(model,'ftrStats')
        if isfield(model.ftrStats,'std')
            testftrs_norm = zeros(size(Xftrs));
            for f = 1:size(Xftrs,1)
                if iscell(model.ftrStats.mean)
                    %             test_input(:,f) = zscore(testfeats(:,f));
                    testftrs_norm(f,:) = bsxfun(@minus,Xftrs(f,:),model.ftrStats.mean{class}(f));
                    testftrs_norm(f,:) = bsxfun(@rdivide,testftrs_norm(f,:),model.ftrStats.std{class}(f));
                else
                    testftrs_norm(f,:) = bsxfun(@minus,Xftrs(f,:),model.ftrStats.mean(f));
                    testftrs_norm(f,:) = bsxfun(@rdivide,testftrs_norm(f,:),model.ftrStats.std(f));
                end
            end
        end
    end
    
    
    %% Run Model
    % Feature Selection
    if isfield(model.ftrStats,'selected')
        testftrs_norm = testftrs_norm(model.ftrStats.selected,:);
    end
    
    % Classify Test Data
    if strcmp(opt.classify.classifier,'mlp')
        [er,bad,Yhat] = nntest(model,testftrs_norm', targtovec(double(testlabels)));
    elseif strcmp(opt.classify.classifier,'svm')
        Yhat = svmclassify(model,testftrs_norm');
%         Yhat(isnan(Yhat)) = 1;
%         Yhat = -(Yhat-1);
    elseif strcmp(opt.classify.classifier,'lda')
        Yhat = (model.const + testftrs_norm'*model.linear) > 0;
    elseif strcmp(opt.classify.classifier,'qda')
        %         Yhat = (model.const + testftrs_norm'*model.linear + ...
        %             testftrs_norm'*model.quadratic*testftrs_norm) > 0;
        Yhat = zeros(nWins,1);
        linterm = model.const + testftrs_norm'*model.linear;
        for t = 1:nWins
            Yhat(t) = (linterm(t) + ...
                testftrs_norm(:,t)'*model.quadratic*testftrs_norm(:,t)) > 0;
        end
    end
    
    % Convert classification results to time-series
    Ytimes = zeros(length(Xtest),1);
    Ytimes(1:TStest(1)) = Yhat(1);
    for ep = 2:nWins-1
        Ytimes(TStest(ep-1):TStest(ep)) = Yhat(ep);
        if (Yhat(ep)~=Yhat(ep-1))&&(Yhat(ep)~=Yhat(ep+1))
            Ytimes(TStest(ep-1):TStest(ep)) = Yhat(ep-1);
        end
    end
    Ytimes(TStest(ep):end) = Yhat(nWins);
    retained = mean(Ytimes==0);
    
    %     % Convert test labels to time-series labels if available
    %     if opt.classify.crossval
    %         tslabels = zeros(length(Xtest),1);
    %         tslabels(1:TStest(1)) = model.testlabels(1)-1;
    %         for ep = 2:nWins-1
    %             tslabels(TStest(ep-1):TStest(ep)) = model.testlabels(ep)-1;
    %             if (model.testlabels(ep)~=model.testlabels(ep-1))&&(model.testlabels(ep)~=model.testlabels(ep+1))
    %                 tslabels(TStest(ep-1):TStest(ep)) = Yhat(ep-1)-1;
    %             end
    %         end
    %     end
    
    Result.Yhat{ts,chan} = Yhat;
    Result.Ytimes{ts,chan} = Ytimes;
    if exist('Y','var')
        Result.Ytrue{ts,chan} = Y;
    end
    Result.model_retained(ts,chan) = retained;
    
    %% PLOTS
    for ch = chan
        if opt.plot.testVapp
            %% Output of current app version versus Current Model
            h = figure;
            
            do_microVolts_conv = (IXDATA.raw.eeg.data(:) == floor(IXDATA.raw.eeg.data(:)));
            if all(do_microVolts_conv) % convert to micro-volts
                SNR_threshold = 150;
            else
                SNR_threshold = 150*(1.644980342484907^2);
                if any(do_microVolts_conv)
                    % 	        disp(['ERROR #9 : Part of EEG data is not in micro Volts for ' thisSessionFilePath]);
                end
            end
            filtInfo = filter_2_36_bp_order6();
            IXDATA = computeSigPower(IXDATA,'thres',SNR_threshold,'filter',[filtInfo.A; filtInfo.B],'window',256);
            badIX = IXDATA.raw.eeg.badsections{chan};
            %     retainedAPP = 0;
            APPtimes = zeros(size(Ytimes));
            for bb = 1:size(badIX,1)
                [xq,badtimesIX(bb,1)] = min(abs(badIX(bb,1)-TS));
                [xq,badtimesIX(bb,2)] = min(abs(badIX(bb,2)-TS));
                APPtimes(badtimesIX(bb,1):badtimesIX(bb,2)) = 1;
                %         retainedAPP = retainedAPP + length(badtimesIX(bb,1):badtimesIX(bb,2));
            end
            %     retainedAPP = 1 - (retainedAPP / length(TS));
            retainedAPP = 1 - mean(APPtimes);
            MLP_APP_agreement = mean(APPtimes==Ytimes);

            % Plot Classifier output for MLP and APP version
            sh(1) = subplot(5,1,1);
            goodraw = Xtest; goodraw(Ytimes==1) = nan;
            badraw = Xtest; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('MLP classification output')
            
            sh(2) = subplot(5,1,2);
            goodlabels = Xtest; goodlabels(APPtimes==1) = nan;
            badlabels = Xtest; badlabels(APPtimes==0) = nan;
            plot(TS-TS(1),goodlabels,'b');
            hold on;
            plot(TS-TS(1),badlabels,'r')
            title('APP VERSION classification output')
            
            % Plot predicted labels
            sh(3) = subplot(5,1,3);
            plot(TStest2,Yhat)
            ylim([0,3])
            linkaxes(sh,'x')
            
            % Plot FFTs
            if length(size(Xfft)) == 2
                Xfft_clean = log10(mean(Xfft(:,Yhat==1),2));
                Xfft_bad = log10(mean(Xfft(:,Yhat==2),2));
                Xfft_mean = log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_clean = log10(mean(Xfft(:,:,Yhat==1),3));
                Xfft_bad = log10(mean(Xfft(:,:,Yhat==2),3));
                Xfft_mean = log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            subplot(5,1,4:5);
            plot(Fr,Xfft_mean(:,1:length(chan)),'b')
            hold on
            plot(Fr,Xfft_clean(:,1:length(chan)),'g')
            hold on
            plot(Fr,Xfft_bad(:,1:length(chan)),'r')
            hold on
            viewFft(IXDATA,'chans',ch,'hold',1,'color','k','clean',1);
            legend('Total','Clean','Bad','APP')
            title('FFTs')
            
            fprintf('MLP_retained: %g APP_retained: %g Agreement: %g \n',...
                retained,retainedAPP,MLP_APP_agreement);
            
            
            %         Result.TStestlabes{ts,chan} = tslabels;
            %         Result.WinErr(ts,chan) = mean(Yhat~=model.testlabels);
            %         Result.TSErr(ts,chan) = mean(Ytimes~=tslabels);
            
            Result.APP_retained(ts,chan) = retainedAPP;
            Result.MLP_APP_agreement(ts,chan) = MLP_APP_agreement;
        end
        
        if opt.plot.testVlabels && opt.classify.crossval
            %% Output of Current Model versus Labeled Data
            h2 = figure;
            
            % Identify samples which do not belong to relevant classes
            rmclass = setxor(class,[2,3,4]);
            rmtimes = zeros(length(Ytimes),1);
            for rm = rmclass
                rmtimes = rmtimes|Y(rm,:)';
            end
            rmtimes = Y(1,:)|Y(class,:);
            
            % Plot Classifier output for MLP and APP version
            sh2(1) = subplot(4,1,1);
            goodraw = Xtest; goodraw(Ytimes==1) = nan;
            badraw = Xtest; badraw(Ytimes==0) = nan;
            plot(TS-TS(1),goodraw,'b');
            hold on;
            plot(TS-TS(1),badraw,'r')
            title('Model Classification Output')
            
            sh2(2) = subplot(4,1,2);
            goodlabels = Xtest; goodlabels(Y(class,:)==1) = nan;
            badlabels = Xtest; badlabels(Y(class,:)==0) = nan;
            rmlabels = Xtest; rmlabels(rmtimes,:) = nan;
            plot(TS-TS(1),goodlabels,'b');
            hold on;
            plot(TS-TS(1),badlabels,'r')
            plot(TS-TS(1),rmlabels,'c')
            title('True Labels')
            
            % Plot FFTs
            if length(size(Xfft)) == 2
                Xfft_model = log10(mean(Xfft(:,Yhat==1),2));
                Xfft_true = log10(mean(Xfft(:,testlabels==0),2));
                Xfft_mean = log10(mean(Xfft,2));
            elseif length(size(Xfft)) == 3
                Xfft_model = log10(mean(Xfft(:,:,Yhat==1),3));
                Xfft_true = log10(mean(Xfft(:,:,testlabels==0),3));
                Xfft_mean = log10(mean(Xfft,3));
            end
            Fr = linspace(0,FS/2,size(Xfft,1));
            
            sh2(3) = subplot(4,1,3:4);
            plot(Fr,Xfft_mean(:,1:length(chan)),'k')
            hold on
            plot(Fr,Xfft_model(:,1:length(chan)),'b')
            hold on
            plot(Fr,Xfft_true(:,1:length(chan)),'g')
            legend('Total','Model','True')
            title('FFTs')
            linkaxes(sh2(1:2),'xy')
            
            Result.fft_corr = corr(Xfft_model,Xfft_true);
            fprintf('FFT Correlation: %g\n',Result.fft_corr);
            
            if opt.plot.barfft
               figure;
               bar(Fr,Xfft_mean,1,'b','BaseValue',-4); hold on
               bar(Fr,Xfft_model,1,'g','BaseValue',-4);
            end
            
        end
        
%         if opt.plot.adjustlabels && opt.classify.crossval
        if opt.classify.crossval
            %% Output of Current Model versus Labeled Data
            h3 = figure;
            
            % Identify samples which do not belong to relevant classes
            rmclass = setxor(class,[2,3,4]);
            rmtimes = zeros(length(Ytimes),1);
            for rm = rmclass
                rmtimes = rmtimes|Y(rm,:)';
            end
            rmtimes = Y(1,:)|Y(class,:);
            
            % Plot Classifier output for MLP and APP version
            sh3(1) = subplot(4,1,1);
            goodraw = Xtest; goodraw(Ytimes==1) = nan;
            badraw = Xtest; badraw(Ytimes==0) = nan;
            plot(goodraw,'b');
            hold on;
            plot(badraw,'r')
            title('Model Classification Output')
            
            sh3(2) = subplot(4,1,2);
            goodlabels = Xtest; goodlabels(Y(class,:)==1) = nan;
            badlabels = Xtest; badlabels(Y(class,:)==0) = nan;
            rmlabels = Xtest; rmlabels(rmtimes,:) = nan;
            plot(goodlabels,'b');
            hold on;
            plot(badlabels,'r')
            plot(rmlabels,'c')
            title('True Labels')
            
            % Plot Ytimes
            sh3(3) = subplot(4,1,3);
            Yplot = zeros(1,length(Y));
            for pp = 2:4; Yplot(Y(pp,:)==1) = pp; end;
            plot(Yplot)
            ylim([-1,5])
            linkaxes(sh3(1:2),'xy')
            linkaxes(sh3(1:3),'x')
        end
        
%         if opt.plot.TestAllModels
%             %% Output of Model built with each Classifier
%             
%         end
        
    end
end